<?php
return array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:hib_tourenplanung/Resources/Private/Language/locallang_db.xlf:tx_hibtourenplanung_domain_model_tourenleiter',
		'label' => 'name',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'sortby' => 'sorting',
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'searchFields' => 'name,email,address,phone,picture,',
		'iconfile' => 'EXT:hib_tourenplanung/Resources/Public/Icons/tx_hibtourenplanung_domain_model_tourenleiter.svg'
	),
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, inactive, type, youth, snow, feuser, name, email, address, phone, emergency, function, picture',
	),
	'types' => array(
		'1' => array('showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1, type, inactive, youth, snow, feuser, name, email, address, phone,emergency, function, picture, --div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access, starttime, endtime'),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(
	
		'sys_language_uid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
			'config' => array(
				'type' => 'select',
				'renderType' => 'selectSingle',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'renderType' => 'selectSingle',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => 'tx_hibtourenplanung_domain_model_tourenleiter',
				'foreign_table_where' => 'AND tx_hibtourenplanung_domain_model_tourenleiter.pid=###CURRENT_PID### AND tx_hibtourenplanung_domain_model_tourenleiter.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),

		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),
	
		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),
		'starttime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'endtime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'type' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:hib_tourenplanung/Resources/Private/Language/locallang_db.xlf:tx_hibtourenplanung_domain_model_tourenleiter.type',
			'config' => array(
				'type' => 'select',
				'renderType' => 'selectSingle',
				'items' => array(
					array('Tourenleiter', '1'),
					array('Tourenleiter & Vorstand', '2'),
					array('Vorstand', '3'),
				        array('Nicht Tourenleiter (Nur Schneesport)', '4'),
				),
				'size' => 1,
				'maxitems' => 1,
				'eval' => ''
			),
		),
		'youth' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:hib_tourenplanung/Resources/Private/Language/locallang_db.xlf:tx_hibtourenplanung_domain_model_tourenleiter.youth',
			'config' => array(
				'type' => 'check',
				'default' => 0
			),
		),
		'inactive' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:hib_tourenplanung/Resources/Private/Language/locallang_db.xlf:tx_hibtourenplanung_domain_model_tourenleiter.inactive',
			'config' => array(
				'type' => 'check',
				'default' => 0
			),
		),
	        'snow' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:hib_tourenplanung/Resources/Private/Language/locallang_db.xlf:tx_hibtourenplanung_domain_model_tourenleiter.snow',
			'config' => array(
				'type' => 'check',
				'default' => 0
			),
		),
	        'feuser' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:hib_tourenplanung/Resources/Private/Language/locallang_db.xlf:tx_hibtourenplanung_domain_model_tourenleiter.feuser',
			'config' => array(
				'type' => 'select',
				'renderType' => 'selectSingle',
				'items' => array(
				    array(' --- Bitte wählen --- ',null)
                                ),
				'foreign_table' => 'fe_users',
				'foreign_table_where' => 'ORDER BY name desc',
				'minitems' => 0,
				'maxitems' => 1,
			),
		),
		'name' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:hib_tourenplanung/Resources/Private/Language/locallang_db.xlf:tx_hibtourenplanung_domain_model_tourenleiter.name',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim,required'
			),
		),
		'email' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:hib_tourenplanung/Resources/Private/Language/locallang_db.xlf:tx_hibtourenplanung_domain_model_tourenleiter.email',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'address' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:hib_tourenplanung/Resources/Private/Language/locallang_db.xlf:tx_hibtourenplanung_domain_model_tourenleiter.address',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 5,
				'eval' => 'trim'
			)
		),
		'phone' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:hib_tourenplanung/Resources/Private/Language/locallang_db.xlf:tx_hibtourenplanung_domain_model_tourenleiter.phone',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
	        'emergency' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:hib_tourenplanung/Resources/Private/Language/locallang_db.xlf:tx_hibtourenplanung_domain_model_tourenleiter.emergency',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
	    	'function' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:hib_tourenplanung/Resources/Private/Language/locallang_db.xlf:tx_hibtourenplanung_domain_model_tourenleiter.function',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'picture' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:hib_tourenplanung/Resources/Private/Language/locallang_db.xlf:tx_hibtourenplanung_domain_model_tourenleiter.picture',
			'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
				'picture',
				array(
					'appearance' => array(
						'createNewRelationLinkTitle' => 'LLL:EXT:cms/locallang_ttc.xlf:images.addFileReference'
					),
					'foreign_types' => array(
						'0' => array(
							'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
						),
						\TYPO3\CMS\Core\Resource\File::FILETYPE_TEXT => array(
							'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
						),
						\TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => array(
							'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
						),
						\TYPO3\CMS\Core\Resource\File::FILETYPE_AUDIO => array(
							'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
						),
						\TYPO3\CMS\Core\Resource\File::FILETYPE_VIDEO => array(
							'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
						),
						\TYPO3\CMS\Core\Resource\File::FILETYPE_APPLICATION => array(
							'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
						)
					),
					'maxitems' => 1
				),
				$GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']
			),
		),
		
	),
);