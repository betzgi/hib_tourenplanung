<?php
return array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:hib_tourenplanung/Resources/Private/Language/locallang_db.xlf:tx_hibtourenplanung_domain_model_report',
		'label' => 'tour',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'sortby' => 'sorting',
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'searchFields' => 'tour,title,shorttext,text,state,author,picture,participant,',
		'iconfile' => 'EXT:hib_tourenplanung/Resources/Public/Icons/tx_hibtourenplanung_domain_model_report.gif'
	),
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, tour, executed, shorttext, title, text, state, author, sent, picture, participant',
	),
	'types' => array(
		'1' => array('showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1, tour, executed, shorttext, title, text, state, author, sent, picture, participant, --div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access, starttime, endtime'),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(
	
		'sys_language_uid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
			'config' => array(
				'type' => 'select',
				'renderType' => 'selectSingle',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'renderType' => 'selectSingle',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => 'tx_hibtourenplanung_domain_model_report',
				'foreign_table_where' => 'AND tx_hibtourenplanung_domain_model_report.pid=###CURRENT_PID### AND tx_hibtourenplanung_domain_model_report.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),

		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),
	
		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),
		'starttime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'endtime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'tour' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:hib_tourenplanung/Resources/Private/Language/locallang_db.xlf:tx_hibtourenplanung_domain_model_report.tour',
			'config' => array(
				'type' => 'select',
				'renderType' => 'selectSingle',
				'items' => array(
				    array(' --- Bitte wählen --- ',null)
                                ),
				'foreign_table' => 'tx_hibtourenplanung_domain_model_tour',
				'foreign_table_where' => 'ORDER BY startdate desc',
				'minitems' => 0,
				'maxitems' => 1,
				'eval' => 'required',
			),
		),
		'executed' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:hib_tourenplanung/Resources/Private/Language/locallang_db.xlf:tx_hibtourenplanung_domain_model_report.executed',
			'config' => array(
				'type' => 'select',
				'renderType' => 'selectSingle',
				'items' => array(
					array('Abgesagt', 0),
					array('Durchgeführt', 1),
				),
				'size' => 1,
				'maxitems' => 1,
				'eval' => ''
			),
		),
		'title' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:hib_tourenplanung/Resources/Private/Language/locallang_db.xlf:tx_hibtourenplanung_domain_model_report.title',
			'config' => array(
				'type' => 'input',
				'size' => 50,
				'eval' => 'trim'
			),
		),
		'shorttext' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:hib_tourenplanung/Resources/Private/Language/locallang_db.xlf:tx_hibtourenplanung_domain_model_report.shorttext',
			'config' => array(
				'type' => 'text',
				'cols' => 50,
				'rows' => 5,
				'eval' => 'trim'
			)
		),
		'text' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:hib_tourenplanung/Resources/Private/Language/locallang_db.xlf:tx_hibtourenplanung_domain_model_report.text',
			'config' => array(
				'type' => 'text',
				'cols' => 50,
				'rows' => 15,
				'eval' => 'trim'
			)
		),
		'state' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:hib_tourenplanung/Resources/Private/Language/locallang_db.xlf:tx_hibtourenplanung_domain_model_report.state',
			'config' => array(
				'type' => 'select',
				'renderType' => 'selectSingle',
				'items' => array(
					array('nicht Freigegeben', 0),
					array('Freigegeben', 1),
				),
				'size' => 1,
				'maxitems' => 1,
				'eval' => ''
			),
		),
	    	'sent' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:hib_tourenplanung/Resources/Private/Language/locallang_db.xlf:tx_hibtourenplanung_domain_model_report.sent',
			'config' => array(
				'readOnly' => 1,
				'type' => 'select',
				'renderType' => 'selectSingle',
				'items' => array(
					array('Email an Tourenchef noch nicht gesendet', 0),
					array('Email an Tourenchef gesendet', 1),
				),
				'size' => 1,
				'maxitems' => 1,
				'eval' => ''
			),
		),
		'author' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:hib_tourenplanung/Resources/Private/Language/locallang_db.xlf:tx_hibtourenplanung_domain_model_report.author',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'picture' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:hib_tourenplanung/Resources/Private/Language/locallang_db.xlf:tx_hibtourenplanung_domain_model_report.picture',
			'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
				'picture',
				array(
					'appearance' => array(
						'createNewRelationLinkTitle' => 'LLL:EXT:cms/locallang_ttc.xlf:images.addFileReference'
					),
					'foreign_types' => array(
						'0' => array(
							'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
						),
						\TYPO3\CMS\Core\Resource\File::FILETYPE_TEXT => array(
							'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
						),
						\TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => array(
							'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
						),
						\TYPO3\CMS\Core\Resource\File::FILETYPE_AUDIO => array(
							'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
						),
						\TYPO3\CMS\Core\Resource\File::FILETYPE_VIDEO => array(
							'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
						),
						\TYPO3\CMS\Core\Resource\File::FILETYPE_APPLICATION => array(
							'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
						)
					),
					'maxitems' => 100
				),
				$GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']
			),
		),
		'participant' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:hib_tourenplanung/Resources/Private/Language/locallang_db.xlf:tx_hibtourenplanung_domain_model_report.participant',
			'config' => array(
				'type' => 'inline',
				'foreign_table' => 'tx_hibtourenplanung_domain_model_participant',
				'foreign_field' => 'report',
				'foreign_sortby' => 'sorting',
				'maxitems' => 9999,
				'appearance' => array(
					'collapseAll' => 1,
					'levelLinksPosition' => 'top',
					'showSynchronizationLink' => 1,
					'showPossibleLocalizationRecords' => 1,
					'useSortable' => 1,
					'showAllLocalizationLink' => 1
				),
			),

		),
		
	),
);