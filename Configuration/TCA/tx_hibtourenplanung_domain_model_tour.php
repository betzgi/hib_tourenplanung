<?php
return array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:hib_tourenplanung/Resources/Private/Language/locallang_db.xlf:tx_hibtourenplanung_domain_model_tour',
		'label' => 'title',
		'label_alt' => 'startdate, tourenleiter',
		'label_alt_force' => 1,
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
	        'default_sortby' => 'ORDER BY startdate DESC, sorting DESC',
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'searchFields' => 'title,unit,startdate,enddate,difficulty,description,requirements,equipment,meetingtime,meeting,returningtime,signin,info,cost,special,state,tourenleiter,',
		'iconfile' => 'EXT:hib_tourenplanung/Resources/Public/Icons/tx_hibtourenplanung_domain_model_tour.gif'
	),
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, title, state, unit, difficulty,eregister, tourenleiter, startdate, enddate,  description, requirements, equipment, meeting, returningtime, signin, info, cost, special, picture, subscriber, whiterisk',
	),
	'types' => array(
		'1' => array('showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1, title, state, unit, difficulty,eregister, tourenleiter, startdate, enddate,  description, whiterisk, requirements, equipment, meeting, returningtime, signin, info, cost, special, picture, subscriber, --div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access, starttime, endtime'),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(
	
		'sys_language_uid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
			'config' => array(
				'type' => 'select',
				'renderType' => 'selectSingle',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'renderType' => 'selectSingle',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => 'tx_hibtourenplanung_domain_model_tour',
				'foreign_table_where' => 'AND tx_hibtourenplanung_domain_model_tour.pid=###CURRENT_PID### AND tx_hibtourenplanung_domain_model_tour.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),

		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),
	
		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),
		'starttime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'endtime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),

		'title' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:hib_tourenplanung/Resources/Private/Language/locallang_db.xlf:tx_hibtourenplanung_domain_model_tour.title',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim,required'
			),
		),
		'startdate' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:hib_tourenplanung/Resources/Private/Language/locallang_db.xlf:tx_hibtourenplanung_domain_model_tour.startdate',
			'config' => array(
				'dbType' => 'date',
				'type' => 'input',
				'size' => 7,
				'eval' => 'date,required',
				'checkbox' => 0,
				'default' => '0000-00-00'
			),
		),
		'enddate' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:hib_tourenplanung/Resources/Private/Language/locallang_db.xlf:tx_hibtourenplanung_domain_model_tour.enddate',
			'config' => array(
				'dbType' => 'date',
				'type' => 'input',
				'size' => 7,
				'eval' => 'date',
				'checkbox' => 0,
				'default' => '0000-00-00'
			),
		),
		'difficulty' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:hib_tourenplanung/Resources/Private/Language/locallang_db.xlf:tx_hibtourenplanung_domain_model_tour.difficulty',
			'config' => array(
				'type' => 'select',
				'renderType' => 'selectSingle',
				'items' => array(
					array('undefiniert', ''),
					array('Wandern (T1)', '(W)-T1'),
					array('Bergwandern (T2)', '(BW)-T2'),
					array('Bergwandern (T3)', '(BW)-T3'),
					array('Alpinwandern (T4)', '(BW)-T4'),
					array('Anspruchsvolles Alpinwandern (T5)', '(BW)-T5'),
					array('Skitour: leicht (L)', '(S)-L'),
					array('Skitour: wenig schwierig (WS)', '(S)-WS'),
					array('Skitour: ziemlich schwierig (ZS)', '(S)-ZS'),
					array('Skitour: schwierig (S)', '(S)-S'),
					array('Ski/Snowboardtour: leicht (L)', '(S/SB)-L'),
					array('Ski/Snowboardtour: wenig schwierig (WS)', '(S/SB)-WS'),
					array('Ski/Snowboardtour: ziemlich schwierig (ZS)', '(S/SB)-ZS'),
					array('Ski/Snowboardtour: schwierig (S)', '(S/SB)-S'),
					array('Schneeschuhtour: leichte Schneeschuhwanderung (WT1)', '(SL)-WT1'),
					array('Schneeschuhtour: Schneeschuhwanderung (WT2)', '(SL)-WT2'),
					array('Schneeschuhtour: anspruchsvolle Schneeschuhwanderung (WT3)', '(SL)-WT3'),
					array('Schneeschuhtour: Schneeschuhtour (WT4)', '(SL)-WT4'),
					array('Schneeschuhtour: alpine Schneeschuhtour (WT5)', '(SL)-WT5'),
					array('Hochtour: leicht (L)', '(HT)-L'),
					array('Hochtour: wenig schwierig (WS)', '(HT)-WS'),
					array('Hochtour: ziemlich schwierig (ZS)', '(HT)-ZS'),
					array('Hochtour: schwierig (S)', '(HT)-S'),
					array('Klettersteig: leicht (KS1)', '(KS)-KS1'),
					array('Klettersteig: mittel (KS2)', '(KS)-KS2'),
					array('Klettersteig: ziemlich schwierig (KS3)', '(KS)-KS3'),
					array('Klettersteig: schwierig (KS4)', '(KS)-KS4'),
					array('Mountainbike: S0', '(MTB)-S0'),
					array('Mountainbike: S1', '(MTB)-S1'),
					array('Mountainbike: S2', '(MTB)-S2'),
					array('Mountainbike: S3', '(MTB)-S3'),
					array('Klettern', '(K)-'),
					array('Kanu', '(KA)-'),
				),
				'size' => 1,
				'maxitems' => 1,
				'eval' => ''
			),
		),
	        'whiterisk' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:hib_tourenplanung/Resources/Private/Language/locallang_db.xlf:tx_hibtourenplanung_domain_model_tour.whiterisk',
			'config' => array(
				'type' => 'input',
				'size' => 50,
				'eval' => 'trim'
			),
		),
		'description' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:hib_tourenplanung/Resources/Private/Language/locallang_db.xlf:tx_hibtourenplanung_domain_model_tour.description',
			'config' => array(
				'type' => 'text',
				'cols' => 50,
				'rows' => 10,
				'eval' => 'trim'
			)
		),
		'requirements' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:hib_tourenplanung/Resources/Private/Language/locallang_db.xlf:tx_hibtourenplanung_domain_model_tour.requirements',
			'config' => array(
				'type' => 'input',
				'size' => 50,
				'eval' => 'trim'
			),
		),
		'equipment' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:hib_tourenplanung/Resources/Private/Language/locallang_db.xlf:tx_hibtourenplanung_domain_model_tour.equipment',
			'config' => array(
				'type' => 'input',
				'size' => 50,
				'eval' => 'trim'
			),
		),
		'meeting' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:hib_tourenplanung/Resources/Private/Language/locallang_db.xlf:tx_hibtourenplanung_domain_model_tour.meeting',
			'config' => array(
				'type' => 'input',
				'size' => 50,
				'eval' => 'trim'
			),
		),
		'returningtime' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:hib_tourenplanung/Resources/Private/Language/locallang_db.xlf:tx_hibtourenplanung_domain_model_tour.returningtime',
			'config' => array(
				'type' => 'input',
				'size' => 50,
				'eval' => 'trim'
			),
		),
		'signin' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:hib_tourenplanung/Resources/Private/Language/locallang_db.xlf:tx_hibtourenplanung_domain_model_tour.signin',
			'config' => array(
				'type' => 'input',
				'size' => 50,
				'eval' => 'trim'
			),
		),
		'info' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:hib_tourenplanung/Resources/Private/Language/locallang_db.xlf:tx_hibtourenplanung_domain_model_tour.info',
			'config' => array(
				'type' => 'input',
				'size' => 50,
				'eval' => 'trim'
			),
		),
		'cost' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:hib_tourenplanung/Resources/Private/Language/locallang_db.xlf:tx_hibtourenplanung_domain_model_tour.cost',
			'config' => array(
				'type' => 'input',
				'size' => 50,
				'eval' => 'trim'
			),
		),
		'special' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:hib_tourenplanung/Resources/Private/Language/locallang_db.xlf:tx_hibtourenplanung_domain_model_tour.special',
			'config' => array(
				'type' => 'input',
				'size' => 50,
				'eval' => 'trim'
			),
		),
		'state' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:hib_tourenplanung/Resources/Private/Language/locallang_db.xlf:tx_hibtourenplanung_domain_model_tour.state',
			'config' => array(
				'type' => 'select',
				'renderType' => 'selectSingle',
				'items' => array(
					array('in Planung (nicht auf Webseite)', 0),
					array('öffentlich (auf Webseite)', 1),
				),
				'size' => 1,
				'maxitems' => 1,
				'eval' => ''
			),
		),
		'eregister' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:hib_tourenplanung/Resources/Private/Language/locallang_db.xlf:tx_hibtourenplanung_domain_model_tour.eregister',
			'config' => array(
				'type' => 'check',
				'default' => 0
			),
		),
		'tourenleiter' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:hib_tourenplanung/Resources/Private/Language/locallang_db.xlf:tx_hibtourenplanung_domain_model_tour.tourenleiter',
			'config' => array(
				'type' => 'select',
				'renderType' => 'selectSingle',
				'items' => array(
				    array(' --- Bitte wählen --- ',null)
                                ),
				'foreign_table' => 'tx_hibtourenplanung_domain_model_tourenleiter',
				'minitems' => 0,
				'maxitems' => 1,
				'eval' => 'required',
			),
		),
		'picture' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:hib_tourenplanung/Resources/Private/Language/locallang_db.xlf:tx_hibtourenplanung_domain_model_tour.picture',
			'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
			'picture', array(
			    'appearance' => array(
				'createNewRelationLinkTitle' => 'LLL:EXT:cms/locallang_ttc.xlf:images.addFileReference'
			    ),
			    'foreign_types' => array(
				'0' => array(
				    'showitem' => '
								    --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
								    --palette--;;filePalette'
				),
				\TYPO3\CMS\Core\Resource\File::FILETYPE_TEXT => array(
				    'showitem' => '
								    --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
								    --palette--;;filePalette'
				),
				\TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => array(
				    'showitem' => '
								    --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
								    --palette--;;filePalette'
				),
				\TYPO3\CMS\Core\Resource\File::FILETYPE_AUDIO => array(
				    'showitem' => '
								    --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
								    --palette--;;filePalette'
				),
				\TYPO3\CMS\Core\Resource\File::FILETYPE_VIDEO => array(
				    'showitem' => '
								    --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
								    --palette--;;filePalette'
				),
				\TYPO3\CMS\Core\Resource\File::FILETYPE_APPLICATION => array(
				    'showitem' => '
								    --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
								    --palette--;;filePalette'
				)
			    ),
			    'maxitems' => 1
				), $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']
		    ),
		),
		'unit' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:hib_tourenplanung/Resources/Private/Language/locallang_db.xlf:tx_hibtourenplanung_domain_model_tour.unit',
			'config' => array(
				'type' => 'select',
				'renderType' => 'selectSingle',
				'items' => array(
					array('Bergfreunde', '1'),
					array('Jugendgruppe', '2'),
					array('Bergfreunde & Jugendgruppe', '3'),
				),
				'size' => 1,
				'maxitems' => 1,
				'eval' => ''
			),
		    ),
		    'subscriber' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:hib_tourenplanung/Resources/Private/Language/locallang_db.xlf:tx_hibtourenplanung_domain_model_tour.subscriber',
			'config' => array(
				'type' => 'inline',
				'foreign_table' => 'tx_hibtourenplanung_domain_model_participant',
				'foreign_field' => 'tour',
				'foreign_sortby' => 'sorting',
				'maxitems' => 9999,
				'appearance' => array(
					'collapseAll' => 1,
					'levelLinksPosition' => 'top',
					'showSynchronizationLink' => 1,
					'showPossibleLocalizationRecords' => 1,
					'useSortable' => 1,
					'showAllLocalizationLink' => 1
				),
			),

		),
		
	),
);