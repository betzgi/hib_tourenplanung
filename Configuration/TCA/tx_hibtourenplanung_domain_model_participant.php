<?php
return array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:hib_tourenplanung/Resources/Private/Language/locallang_db.xlf:tx_hibtourenplanung_domain_model_participant',
		'label' => 'name',
	    	'label_alt' => 'email, phone',
		'label_alt_force' => 1,
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'sortby' => 'sorting',
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'searchFields' => 'name,sex,',
		'iconfile' => 'EXT:hib_tourenplanung/Resources/Public/Icons/tx_hibtourenplanung_domain_model_participant.svg'
	),
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, name, place, car, youth, feuser, leader, sex, email, phone, emergency, message',
	),
	'types' => array(
		'1' => array('showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1, name, place, car, youth, feuser, leader, sex, email, phone, emergency, message, --div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access, starttime, endtime'),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(
	
		'sys_language_uid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
			'config' => array(
				'type' => 'select',
				'renderType' => 'selectSingle',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'renderType' => 'selectSingle',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => 'tx_hibtourenplanung_domain_model_participant',
				'foreign_table_where' => 'AND tx_hibtourenplanung_domain_model_participant.pid=###CURRENT_PID### AND tx_hibtourenplanung_domain_model_participant.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),

		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),
	
		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),
		'starttime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'endtime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'pid' => array(
		    'exclude' => 0,
		    'label' => '',
		    'config' => array(
			'type' => 'input',
			'size' => 30,
			'eval' => 'trim'
		    ),
		),

		'name' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:hib_tourenplanung/Resources/Private/Language/locallang_db.xlf:tx_hibtourenplanung_domain_model_participant.name',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'sex' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:hib_tourenplanung/Resources/Private/Language/locallang_db.xlf:tx_hibtourenplanung_domain_model_participant.sex',
			'config' => array(
				'type' => 'select',
				'renderType' => 'selectSingle',
				'items' => array(
					array('', 0),
					array('Frau', 1),
					array('Mann', 2),
				),
				'size' => 1,
				'maxitems' => 1,
				'eval' => ''
			),
		),
		'place' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:hib_tourenplanung/Resources/Private/Language/locallang_db.xlf:tx_hibtourenplanung_domain_model_participant.place',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
	    	'email' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:hib_tourenplanung/Resources/Private/Language/locallang_db.xlf:tx_hibtourenplanung_domain_model_participant.email',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'email'
			),
		),
		'phone' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:hib_tourenplanung/Resources/Private/Language/locallang_db.xlf:tx_hibtourenplanung_domain_model_participant.phone',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
	        'emergency' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:hib_tourenplanung/Resources/Private/Language/locallang_db.xlf:tx_hibtourenplanung_domain_model_participant.emergency',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
	    	'message' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:hib_tourenplanung/Resources/Private/Language/locallang_db.xlf:tx_hibtourenplanung_domain_model_participant.message',
			'config' => array(
				'type' => 'text',
				'cols' => 50,
				'rows' => 5,
				'eval' => 'trim'
			)
		),
	    	'leader' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:hib_tourenplanung/Resources/Private/Language/locallang_db.xlf:tx_hibtourenplanung_domain_model_participant.leader',
			'config' => array(
				'type' => 'check',
				'default' => 0
			),
		),
		'car' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:hib_tourenplanung/Resources/Private/Language/locallang_db.xlf:tx_hibtourenplanung_domain_model_participant.car',
			'config' => array(
				'type' => 'check',
				'default' => 0
			),
		),
	        'youth' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:hib_tourenplanung/Resources/Private/Language/locallang_db.xlf:tx_hibtourenplanung_domain_model_participant.youth',
			'config' => array(
				'type' => 'check',
				'default' => 0
			),
		),
	        'feuser' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:hib_tourenplanung/Resources/Private/Language/locallang_db.xlf:tx_hibtourenplanung_domain_model_participant.feuser',
			'config' => array(
				'type' => 'select',
				'renderType' => 'selectSingle',
				'items' => array(
				    array(' --- Bitte wählen --- ',null)
                                ),
				'foreign_table' => 'fe_users',
				'foreign_table_where' => 'ORDER BY name desc',
				'minitems' => 0,
				'maxitems' => 1,
			),
		),
		'tour' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),
		'report' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),
	),
);