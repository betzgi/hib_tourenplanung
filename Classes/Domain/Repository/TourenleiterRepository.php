<?php
namespace BeatHeim\HibTourenplanung\Domain\Repository;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016 Beat Heim <betzgi@gmail.com>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * The repository for Tourenleiters
 */
class TourenleiterRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{

    /**
     * @var array
     */
    protected $defaultOrderings = array(
        'name' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
    );
    
    /**
    * Finds active tourenleiter 
    *
    */             
    public function findAllActive() {

	$query = $this->createQuery();
	$query->getQuerySettings()->setRespectStoragePage(FALSE);
	$query->matching(
	    $query->logicalAnd(
		$query->equals('hidden', 0),
		$query->equals('deleted', 0),
		$query->equals('inactive', 0),
		$query->logicalOr(
		    $query->equals('type', 1),
		    $query->equals('type', 2)
		)
	    )
	);
	//echo($query);
	return $query->execute();
    }
    
    /**
    * Finds all tourenleiter 
    *
    */             
    public function findAllForEdit() {

	$query = $this->createQuery();
	$query->getQuerySettings()->setRespectStoragePage(FALSE);
	$query->matching(
	    $query->logicalAnd(
		$query->equals('deleted', 0),	
		$query->logicalOr(
		    $query->equals('type', 1),
		    $query->equals('type', 2),
		    $query->equals('type', 4)
		)
	    )
	);
	//echo($query);
	return $query->execute();
    }
    
    /**
    * Finds active tourenleiter (BL/Jugendgruppe)
    *
    */             
    public function findAllActiveType($youth) {

	$query = $this->createQuery();
	$query->getQuerySettings()->setRespectStoragePage(FALSE);
	$query->matching(
	    $query->logicalAnd(
		$query->equals('hidden', 0),
		$query->equals('deleted', 0),
		$query->equals('inactive', 0),
		$query->equals('youth', $youth),
		$query->logicalOr(
		    $query->equals('type', 1),
		    $query->equals('type', 2)
		)
	    )
	);
	//echo($query);
	return $query->execute();
    }
    
    /**
    * Finds active snowsport leiter
    *
    */             
    public function findAllActiveSnow() {

	$query = $this->createQuery();
	$query->getQuerySettings()->setRespectStoragePage(FALSE);
	$query->matching(
	    $query->logicalAnd(
		$query->equals('hidden', 0),
		$query->equals('deleted', 0),
		$query->equals('inactive', 0),
		$query->equals('snow', 1)
	    )
	);
	//echo($query);
	return $query->execute();
    }
    
    /**
    * Finds active vorstand 
    *
    */             
    public function findVorstand() {

	$query = $this->createQuery();
	$query->getQuerySettings()->setRespectStoragePage(FALSE);
	$query->setOrderings (Array('sorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_DESCENDING));
	$query->matching(
	    $query->logicalAnd(
		$query->equals('hidden', 0),
		$query->equals('deleted', 0),
		$query->equals('inactive', 0),
		$query->logicalOr(
		    $query->equals('type', 2),
		    $query->equals('type', 3)
		)
	    )
	);
	//echo($query);
	return $query->execute();
    }
    
    /**
    * Finds by feuser 
    *
    */             
    public function findLogTourenleiter() {

	if (isset($GLOBALS['TSFE']->fe_user->user['uid'])){
	    $feuser = $GLOBALS['TSFE']->fe_user->user['uid'];
	    
	    $query = $this->createQuery();
	    $query->getQuerySettings()->setRespectStoragePage(FALSE);
	    $query->matching(
		$query->logicalAnd(
		    $query->equals('hidden', 0),
		    $query->equals('deleted', 0),
		    $query->equals('inactive', 0),
		    $query->logicalOr(
			$query->equals('type', 1),
			$query->equals('type', 2)
		    )
		)
	    );
	    $leiters = $query->execute();
	    $Uid = '';
	    foreach ($leiters as $leiter) {
		if ($leiter->getFeuser() == $feuser)
		    $Uid =  $leiter->getUid();
	    }
	    return $Uid;
	} else {
	    return '';
	}
    }
}