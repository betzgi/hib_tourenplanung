<?php
namespace BeatHeim\HibTourenplanung\Domain\Repository;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016 Beat Heim <betzgi@gmail.com>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * The repository for Tours
 */
class TourRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{

    /**
     * @var array
     */
    protected $defaultOrderings = array(
        'startdate' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
    );
    
    /**
    * Finds touren of specific year
    *
    */             
    public function findByYear($year,$unit) {
	$date_min = $year."-01-01";
	$date_max = $year."-12-31";
	$query = $this->createQuery();
	$query->getQuerySettings()->setRespectStoragePage(TRUE);
	$query->matching(
	    $query->logicalAnd(
		$query->greaterThanOrEqual('startdate', $date_min),
		$query->lessThanOrEqual('startdate', $date_max),
		$query->equals('state', 1),
		$query->logicalOr(
		    $query->equals('unit', $unit),
		    $query->equals('unit', '3')
		)
	    )
	);
	return $query->execute();
    }
    
    /**
    * puts together statistic
    *
    */             
    public function findStatistic($year) {
	$this->tourenleiterRepository = $this->objectManager->get('BeatHeim\HibTourenplanung\Domain\Repository\TourenleiterRepository');
	$this->reportRepository = $this->objectManager->get('BeatHeim\HibTourenplanung\Domain\Repository\ReportRepository');
	$tourenleiter = $this->tourenleiterRepository->findAllActive();
	
	$date_min = $year."-01-01";
	$date_max = $year."-12-31";
	$statistic = array();
	$totalAll = array();
	for ($i = 1; $i <= 13; $i++)
	{
	    $totalAll[$i]['Ausgeschrieben'] = 0;
	    $totalAll[$i]['Durchgefuehrt'] = 0;
	    $totalAll[$i]['Teilnehmer'] = 0;
	}
	foreach ($tourenleiter as $leiter)
	{
	    $total['Ausgeschrieben'] = 0;
	    $total['Durchgefuehrt'] = 0;
	    $total['Teilnehmer'] = 0;
	    for ($i = 1; $i <= 12; $i++) {
		$month = sprintf("%02s", $i);
		$date_min = $year."-".$month."-01";
		$date_max = $year."-".$month."-31";
		$query = $this->createQuery();
		$query->getQuerySettings()->setRespectStoragePage(FALSE);
		$query->matching(
		    $query->logicalAnd(
			$query->greaterThanOrEqual('startdate', $date_min),
			$query->lessThanOrEqual('startdate', $date_max),
			$query->equals('tourenleiter', $leiter->getUid()),
			$query->equals('hidden', 0),
			$query->equals('deleted', 0)
		    )
		);
		$touren = $query->execute();
		$stat =  array();
		$stat['Ausgeschrieben'] = $touren ->count();
		$totalAll[$i]['Ausgeschrieben'] += $touren ->count();
		$stat['Durchgefuehrt'] = 0;
		$stat['Teilnehmer'] = 0;

		foreach ($touren as $tour)
		{
		    if ($tour->getReportAll())
		    {
			//\TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($tour->getReportAll());
			$report = $this->reportRepository->findByUid($tour->getReportAll()->getUid());
			if ($report->getExecuted() == 1)
			{
			    $stat['Durchgefuehrt'] += 1;
			    $totalAll[$i]['Durchgefuehrt'] += 1;
			}
			$stat['Teilnehmer'] += $report->getParticipant()->count();
			$totalAll[$i]['Teilnehmer'] += $stat['Teilnehmer'];
		    }
		}
		$total['Ausgeschrieben'] += $stat['Ausgeschrieben'];
		$totalAll[13]['Ausgeschrieben'] += $stat['Ausgeschrieben'];
		$total['Durchgefuehrt'] += $stat['Durchgefuehrt'];
		$totalAll[13]['Durchgefuehrt'] += $stat['Durchgefuehrt'];
		$total['Teilnehmer'] += $stat['Teilnehmer'];
		$totalAll[13]['Teilnehmer'] += $stat['Teilnehmer'];
		$statLeiter[$i] = $stat;
	    }
	    $statLeiter[13] = $total;
	    $statistic[$leiter->getName()] = $statLeiter;     
	}
	$statistic['Total'] = $totalAll;
	//\TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($statistic);
	return $statistic;
    }
    
    
    /**
    * Finds touren of specific year (all)
    *
    */             
    public function findByYearProv($year,$unit) {
	$date_min = $year."-01-01";
	$date_max = $year."-12-31";
	$query = $this->createQuery();
	$query->getQuerySettings()->setRespectStoragePage(TRUE);
	
	$query->matching(
	    $query->logicalAnd(
		$query->greaterThanOrEqual('startdate', $date_min),
		$query->lessThanOrEqual('startdate', $date_max),
		$query->logicalOr(
		    $query->equals('unit', $unit),
		    $query->equals('unit', '3')
		)
	    )
	); 

	return $query->execute();
    }
    
    /**
    * Finds touren back one year with no report
    *
    */             
    public function findOneYear() {
	$date_max = date("Y-m-d");
	$date_min = date("Y-m-d", strtotime('-6 month'));
	$query = $this->createQuery();
	$query->getQuerySettings()->setRespectStoragePage(FALSE);
	$query->matching(
	    $query->logicalAnd(
		$query->greaterThanOrEqual('startdate', $date_min),
		$query->lessThanOrEqual('startdate', $date_max),
		$query->equals('state', 1)
	    )
	);
	$result = $query->execute();
	$touren = array();
	foreach ($result as $tour)
	{
	    if ($tour->getReportAll()== NULL )
		$touren[] = $tour;
	}
	return $touren;
    }
    
    /**
    * Finds touren back one year with no report
    *
    */             
    public function findDetailRemind() {

	$checkdate = date("Y-m-d", strtotime('4 month'));  // in 3 Monaten
	$query = $this->createQuery();
	$query->getQuerySettings()->setRespectStoragePage(FALSE);
	$query->matching(
	    $query->logicalAnd(
		$query->equals('startdate', $checkdate)
	    )
	);
	return $query->execute();
    }
    
    
    
    /**
    * Finds latest touren 
    *
    */             
    public function findLatest($unit) {
	$date = new \DateTime('yesterday');
	$query = $this->createQuery();
	$query->getQuerySettings()->setRespectStoragePage(TRUE);
	$query->matching(
	    $query->logicalAnd(
		$query->greaterThanOrEqual('startdate', $date->format('Y-m-d')),
		$query->equals('state', 1),
		    $query->logicalOr(
		    $query->equals('unit', $unit),
		    $query->equals('unit', '3')
		)
	    ),
	$query ->setLimit(10)    
	);
	return $query->execute();
    }
    
        /**
    * Finds latest touren (for mobile)
    *
    */             
    public function findLatestMobile($unit) {
	$date = new \DateTime('yesterday');
	$query = $this->createQuery();
	$query->getQuerySettings()->setRespectStoragePage(TRUE);
	$query->matching(
	    $query->logicalAnd(
		$query->greaterThanOrEqual('startdate', $date->format('Y-m-d')),
		$query->equals('state', 1),
		    $query->equals('state', 1),
		    $query->logicalOr(
			$query->equals('unit', $unit),
			$query->equals('unit', '3')
		    )
	    ),
	$query ->setLimit(20)    
	);
	return $query->execute();
    }
    
    /**
    * Finds touren of specific tourenleiter and year
    *
    */             
    public function findByTourenleiter(\BeatHeim\HibTourenplanung\Domain\Model\Tourenleiter $tourenleiter, $year) {
	$date_min = $year."-01-01";
	$date_max = $year."-12-31";
	$query = $this->createQuery();
	$query->getQuerySettings()->setRespectStoragePage(TRUE);
	$query->matching(
	    $query->logicalAnd(
		$query->greaterThanOrEqual('startdate', $date_min),
		$query->lessThanOrEqual('startdate', $date_max),
		$query->equals('tourenleiter', $tourenleiter->getUid()),
		$query->equals('state', 1)
	    )
	);
	return $query->execute();
    }

    /**
    * Finds touren of specific year (inactive too)
    *
    */             
    public function findByYearInactive($year) {
	$date_min = $year."-01-01";
	$date_max = $year."-12-31";
	$query = $this->createQuery();
	$query->getQuerySettings()->setRespectStoragePage(TRUE);
	$query->matching(
	    $query->logicalAnd(
		$query->greaterThanOrEqual('startdate', $date_min),
		$query->lessThanOrEqual('startdate', $date_max)
	    )
	);
	return $query->execute();
    }
    
    /**
    * Finds all touren with no report
    *
    */             
    public function findAllWithNoReport($tourenMitRep) {


	$query = $this->createQuery();
	$query->getQuerySettings()->setRespectStoragePage(TRUE);
	$query->matching(
	    $query->logicalNot(
		$query->in('uid', $tourenMitRep)
	    )
	);
	return $query->execute();
    }
    
}