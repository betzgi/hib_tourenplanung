<?php
namespace BeatHeim\HibTourenplanung\Domain\Repository;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016 Beat Heim <betzgi@gmail.com>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * The repository for Reports
 */
class ReportRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{

    /**
     * @var array
     */
    protected $defaultOrderings = array(
        'tour.startdate' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
    );

    
    /**
    * Finds reports of specific year
    *
    */             
    public function findByYear($year,$unit) {
	$date_min = $year."-01-01";
	$date_max = $year."-12-31";
	$query = $this->createQuery();
	$query->getQuerySettings()->setRespectStoragePage(TRUE);
	$query->matching(
	    $query->logicalAnd(
		$query->greaterThanOrEqual('tour.startdate', $date_min),
		$query->lessThanOrEqual('tour.startdate', $date_max),
		$query->equals('state', 1),
		$query->equals('executed', 1),
		$query->logicalNot($query->equals('text','')),
		$query->logicalOr(
		    $query->equals('tour.unit', $unit),
		    $query->equals('tour.unit', '3')
		)
	    )
	);
	return $query->execute();
    }
    
    /**
    * Finds reports of specific year that are executed
    *
    */             
    public function findByYearExecuted($year,$unit) {
	$date_min = $year."-01-01";
	$date_max = $year."-12-31";
	$query = $this->createQuery();
	$query->getQuerySettings()->setRespectStoragePage(TRUE);
	$query->matching(
	    $query->logicalAnd(
		$query->greaterThanOrEqual('tour.startdate', $date_min),
		$query->lessThanOrEqual('tour.startdate', $date_max),
		$query->equals('executed', 1)
	    )
	);
	return $query->execute();
    }
    
    /**
    * Finds All reports of specific year
    *
    */             
    public function findAllByYear($year) {
	$date_min = $year."-01-01";
	$date_max = $year."-12-31";
	$query = $this->createQuery();
	$query->getQuerySettings()->setRespectStoragePage(TRUE);
	$query->matching(
	    $query->logicalAnd(
		$query->greaterThanOrEqual('tour.startdate', $date_min),
		$query->lessThanOrEqual('tour.startdate', $date_max)
	    )
	);
	return $query->execute();
    }
    
    /**
    * Finds reports of specific year
    *
    */             
    public function findByYearWithPhoto($year, $unit) {
	$date_min = $year."-01-01";
	$date_max = $year."-12-31";
	$query = $this->createQuery();
	$query->getQuerySettings()->setRespectStoragePage(TRUE);
	$query->matching(
	    $query->logicalAnd(
		$query->greaterThanOrEqual('tour.startdate', $date_min),
		$query->lessThanOrEqual('tour.startdate', $date_max),
		$query->logicalOr(
		    $query->equals('tour.unit', $unit),
		    $query->equals('tour.unit', '3')
		)
	    )
	);
	$result = $query->execute();
	$reports = array();
	foreach ($result as $report)
	{
	    $count = count($report->getPicture());
	    if ($count>=1)
		$reports[] = $report;
	}
	return $reports;
    }
    
    /**
    * Finds reports of specific tour
    *
    */             
    public function findByTour($tourUid) {
	$query = $this->createQuery();
	$query->getQuerySettings()->setRespectStoragePage(FALSE);
	$query->matching(
	    $query->logicalAnd(
		$query->equals('tour', $tourUid),
		$query->equals('state', 1),
		$query->equals('executed', 1)
	    )
	);
	return $query->execute();
    }
    
    /**
    * Finds reports of specific tour (all of them
    *
    */             
    public function findAlwaysByTour($tourUid) {
	$query = $this->createQuery();
	$query->getQuerySettings()->setRespectStoragePage(FALSE);
	$query->matching(
	    $query->logicalAnd(
		$query->equals('tour', $tourUid)
	    )
	);
	return $query->execute();
    }
    
    /**
    * Finds latest report 
    *
    */             
    public function findLatest($unit) {
	$query = $this->createQuery();
	$query->getQuerySettings()->setRespectStoragePage(TRUE);
	$query->setOrderings (Array('crdate' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_DESCENDING));
	$query->matching(
	    $query->logicalAnd(
		$query->equals('hidden', 0),
		$query->logicalOr(
		    $query->equals('tour.unit', $unit),
		    $query->equals('tour.unit', '3')
		)
	    ),
	$query ->getLimit(15)   
	);
	
	// alle reports mit Bilder oder state =1 and text
	$result = $query->execute();
	//\TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($result);
	$reports = array();
	foreach ($result as $report)
	{
	    $count = count($report->getPicture());
	    if ($count>=1)
	    {
		$reports[] = $report;
	    }
	    else
	    {
		if ($report->getState() == 1 && $report->getText() != "")
		    $reports[] = $report;
	    }
	}
	
	return $reports;
    }
}