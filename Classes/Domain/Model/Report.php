<?php
namespace BeatHeim\HibTourenplanung\Domain\Model;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016 Beat Heim <betzgi@gmail.com>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Report
 */
class Report extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * multi (für Mehrfacherfassung)
     *
     * @var string
     */
    protected $multi;
    
    /**
     * title
     *
     * @var string
     */
    protected $title = '';
    
    /**
     * shorttext
     *
     * @var string
     */
    protected $shorttext = '';
    
    /**
     * text
     *
     * @var string
     */
    protected $text = '';
    
    /**
     * state
     *
     * @var int
     */
    protected $state = 0;
    
     /**
     * sent
     *
     * @var int
     */
    protected $sent = 0;
    
     /**
     * executed
     *
     * @var bool
     */
    protected $executed = false;
    
    /**
     * author
     *
     * @var string
     */
    protected $author = '';
    
    /**
     * picture
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference>
     */
    protected $picture;
    
    /**
     * tour
     *
     * @var \BeatHeim\HibTourenplanung\Domain\Model\Tour
     */
    protected $tour = null;
    
    /**
     * participant
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\BeatHeim\HibTourenplanung\Domain\Model\Participant>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $participant = null;
    
    /**
     * Returns the text
     *
     * @return string $text
     */
    public function getText()
    {
        return $this->text;
    }
    
    /**
     * Sets the text
     *
     * @param string $text
     * @return void
     */
    public function setText($text)
    {
        $this->text = $text;
    }
    
    /**
     * Returns the author
     *
     * @return string $author
     */
    public function getAuthor()
    {
        return $this->author;
    }
    
    /**
     * Sets the author
     *
     * @param string $author
     * @return void
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }
    
    /**
     * Returns the tour
     *
     * @return \BeatHeim\HibTourenplanung\Domain\Model\Tour $tour
     */
    public function getTour()
    {
        return $this->tour;
    }
    
    /**
     * Sets the tour
     *
     * @param \BeatHeim\HibTourenplanung\Domain\Model\Tour $tour
     * @return void
     */
    public function setTour(\BeatHeim\HibTourenplanung\Domain\Model\Tour $tour)
    {
        $this->tour = $tour;
    }
    
    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }
    
    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }
    
    /**
     * __construct
     */
    public function __construct()
    {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }
    
    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->participant = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }
    
    /**
     * Returns the picture
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference> picture
     */
    public function getPicture()
    {
        return $this->picture;
    }
    
//    /**
//     * Sets the picture
//     *
//     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $picture
//     * @return void
//     */
//    public function setPicture(\TYPO3\CMS\Extbase\Domain\Model\FileReference $picture)
//    {
//        $this->picture = $picture;
//    }
    
    /**
     * Adds a Participant
     *
     * @param \BeatHeim\HibTourenplanung\Domain\Model\Participant $participant
     * @return void
     */
    public function addParticipant(\BeatHeim\HibTourenplanung\Domain\Model\Participant $participant)
    {
        $this->participant->attach($participant);
    }
    
    /**
     * Removes a Participant
     *
     * @param \BeatHeim\HibTourenplanung\Domain\Model\Participant $participantToRemove The Participant to be removed
     * @return void
     */
    public function removeParticipant(\BeatHeim\HibTourenplanung\Domain\Model\Participant $participantToRemove)
    {
        $this->participant->detach($participantToRemove);
    }
    
    /**
     * Returns the participant
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\BeatHeim\HibTourenplanung\Domain\Model\Participant> $participant
     */
    public function getParticipant()
    {
        return $this->participant;
    }
    
    /**
     * Sets the participant
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\BeatHeim\HibTourenplanung\Domain\Model\Participant> $participant
     * @return void
     */
    public function setParticipant(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $participant)
    {
        $this->participant = $participant;
    }
    
    /**
     * Returns the shorttext
     *
     * @return string $shorttext
     */
    public function getShorttext()
    {
        return $this->shorttext;
    }
    
    /**
     * Sets the shorttext
     *
     * @param string $shorttext
     * @return void
     */
    public function setShorttext($shorttext)
    {
        $this->shorttext = $shorttext;
    }
    
    /**
     * Returns the state
     *
     * @return int $state
     */
    public function getState()
    {
        return $this->state;
    }
    
    /**
     * Sets the state
     *
     * @param int $state
     * @return void
     */
    public function setState($state)
    {
        $this->state = $state;
    }
    
     /**
     * Returns the sent
     *
     * @return int $sent
     */
    public function getSent()
    {
        return $this->sent;
    }
    
    /**
     * Sets the sent
     *
     * @param int $sent
     * @return void
     */
    public function setSent($sent)
    {
        $this->sent = $sent;
    }
    
    /**
     * Returns the executed
     *
     * @return bool $executed
     */
    public function getExecuted()
    {
        return $this->executed;
    }
    
    /**
     * Sets the executed
     *
     * @param bool $executed
     * @return void
     */
    public function setExecuted($executed)
    {
        $this->executed = $executed;
    }

    /**
     * Returns the boolean state of executed
     *
     * @return bool $executed
     */
    public function isExecuted()
    {
        return $this->executed;
    }
}