<?php
namespace BeatHeim\HibTourenplanung\Domain\Model;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016 Beat Heim <betzgi@gmail.com>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Participant
 */
class Participant extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    
    /**
     * name
     *
     * @var string
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     */
    protected $name = '';
    
    /**
     * sex
     *
     * @var int
     */
    protected $sex = 0;
    
    /**
     * email
     *
     * @var string
     * @TYPO3\CMS\Extbase\Annotation\Validate("EmailAddressValidator")
     */
    protected $email = '';
    
    /**
     * phone
     *
     * @var string
     */
    protected $phone = '';
    
    /**
     * emergency
     *
     * @var string
     */
    protected $emergency = '';
  
      
    /**
     * place
     *
     * @var string
     */
    protected $place = '';
    
    /**
     * message
     *
     * @var string
     */
    protected $message = '';
    
     /**
     * leader
     *
     * @var bool
     */
    protected $leader = false;
    
     /**
     * car
     *
     * @var bool
     */
    protected $car = false;
    
    /**
     * youth
     *
     * @var bool
     */
    protected $youth = false;
    
    /**
     * feuser
     *
     * @var int
     */
    protected $feuser = 0;
    
    
    /**
     * __construct
     */
    public function __construct()
    {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }
    
    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        
    }
    
    /**
     * Returns the name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Sets the name
     *
     * @param string $name
     * @return void
     */
    public function setName($name)
    {
        $this->name = $name;
    }
    
    /**
     * Returns the sex
     *
     * @return int $sex
     */
    public function getSex()
    {
        return $this->sex;
    }
    
    /**
     * Sets the sex
     *
     * @param int $sex
     * @return void
     */
    public function setSex($sex)
    {
        $this->sex = $sex;
    }
   
    
    /**
     * Returns the email
     *
     * @return string $email
     */
    public function getEmail()
    {
        return $this->email;
    }
    
    /**
     * Sets the email
     *
     * @param string $email
     * @return void
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * Returns the place
     *
     * @return string $place
     */
    public function getPlace()
    {
        return $this->place;
    }
    
    /**
     * Sets the place
     *
     * @param string $place
     * @return void
     */
    public function setPlace($place)
    {
        $this->place = $place;
    }
    
    /**
     * Returns the phone
     *
     * @return string $phone
     */
    public function getPhone()
    {
        return $this->phone;
    }
    
    /**
     * Sets the phone
     *
     * @param string $phone
     * @return void
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }
    
    /**
     * Returns the emergency
     *
     * @return string $emergency
     */
    public function getEmergency()
    {
        return $this->emergency;
    }
    
    /**
     * Sets the emergency
     *
     * @param string $emergency
     * @return void
     */
    public function setEmergency($emergency)
    {
        $this->emergency = $emergency;
    }
    
    /**
     * Returns the message
     *
     * @return string $message
     */
    public function getMessage()
    {
        return $this->message;
    }
    
    /**
     * Sets the message
     *
     * @param string $message
     * @return void
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }
    
     /**
     * Returns the leader
     *
     * @return bool $leader
     */
    public function getLeader()
    {
        return $this->leader;
    }
    
    /**
     * Sets the leader
     *
     * @param bool $leader
     * @return void
     */
    public function setLeader($leader)
    {
        $this->leader = $leader;
    }
    
     /**
     * Returns the car
     *
     * @return bool $car
     */
    public function getCar()
    {
        return $this->car;
    }
    
    /**
     * Sets the car
     *
     * @param bool $car
     * @return void
     */
    public function setCar($car)
    {
        $this->car = $car;
    }
    
    /**
     * Returns the youth
     *
     * @return bool $youth
     */
    public function getYouth()
    {
        return $this->youth;
    }
    
    /**
     * Sets the youth
     *
     * @param bool $youth
     * @return void
     */
    public function setYouth($youth)
    {
        $this->youth = $youth;
    }
    
    /**
     * Returns the feuser
     *
     * @return int $feuser
     */
    public function getFeuser()
    {
        return $this->feuser;
    }
    
    /**
     * Sets the feuser
     *
     * @param int $feuser
     * @return void
     */
    public function setFeuser($feuser)
    {
        $this->feuser = $feuser;
    }
    
    /** 
    * Returns hidden 
    * 
    * @return boolean $hidden 
    */
    public function getHidden() {
	return $this->hidden;
    }


    /**
    * Sets hidden 
    * 
    * @param boolean $hidden 
    * @return void 
    */
    public function setHidden($hidden) {    
	$this->hidden = $hidden;
    }


    /** 
    * Returns the boolean state of hidden 
    * 
    * @return boolean 
    */
    public function isHidden() {    
	return $this->getHidden();
    }
}