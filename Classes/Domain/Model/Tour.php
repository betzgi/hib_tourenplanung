<?php
namespace BeatHeim\HibTourenplanung\Domain\Model;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016 Beat Heim <betzgi@gmail.com>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Tour
 */
class Tour extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * title
     *
     * @var string
     * @TYPO3\CMS\Extbase\Annotation\Validate(validator="NotEmpty")
     */
    protected $title = '';
    
    /**
     * unit
     *
     * @var string
     */
    protected $unit = '';
    
    /**
     * startdate
     *
     * @var \DateTime
     * @TYPO3\CMS\Extbase\Annotation\Validate("\BeatHeim\HibTourenplanung\Validation\Validator\StartdateValidator")
     */
    protected $startdate = null;
    
    /**
     * enddate
     *
     * @var \DateTime
     */
    protected $enddate = null;
    
    /**
     * difficulty
     *
     * @var string
     */
    protected $difficulty = '';
    
    /**
     * description
     *
     * @var string
     */
    protected $description = '';
    
    /**
     * whiterisk
     *
     * @var string
     */
    protected $whiterisk = '';
    
    /**
     * requirements
     *
     * @var string
     */
    protected $requirements = '';
    
    /**
     * equipment
     *
     * @var string
     */
    protected $equipment = '';
    
    
    /**
     * meeting
     *
     * @var string
     */
    protected $meeting = '';
    
    /**
     * returningtime
     *
     * @var string
     */
    protected $returningtime = '';
    
    
    /**
     * signin
     *
     * @var string
     */
    protected $signin = '';
    
    /**
     * info
     *
     * @var string
     */
    protected $info = '';
    
    /**
     * cost
     *
     * @var string
     */
    protected $cost = '';
    
    /**
     * special
     *
     * @var string
     */
    protected $special = '';
    
    /**
     * state
     *
     * @var int
     */
    protected $state = 0;
    
    /**
     * eregister
     *
     * @var bool
     */
    protected $eregister = false;
    
    /**
     * tourenleiter
     *
     * @var \BeatHeim\HibTourenplanung\Domain\Model\Tourenleiter
     */
    protected $tourenleiter = null;
    
    
     /**
     * fotos
     *
     * @var int
     */
    protected $fotos = null;
    
    /**
     * subscriber
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\BeatHeim\HibTourenplanung\Domain\Model\Participant>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $subscriber = null;
    
    /**
     * picture
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     */
    protected $picture = null;
    
    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }
    
    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }
    
    /**
     * Returns the startdate
     *
     * @return \DateTime $startdate
     */
    public function getStartdate()
    {
        return $this->startdate;
    }
    
    /**
     * Sets the startdate
     *
     * @param \DateTime $startdate
     * @return void
     */
    public function setStartdate(\DateTime $startdate)
    {
        $this->startdate = $startdate;
    }
    
    /**
     * __construct
     */
    public function __construct()
    {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }
    
    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->subscriber = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }
    
    
     /**
     * Returns the code of type (from difficulty)
     *
     * @return string $type
     */
    public function getType()
    {
        return substr($this->difficulty,0, strpos($this->difficulty,'-'));
    }
    
    
    /**
     * Sets the unit
     *
     * @param string $unit
     * @return void
     */
    public function setUnit($unit)
    {
        $this->unit = $unit;
    }
    
    /**
     * Returns the unit
     *
     * @return string $unit
     */
    public function getUnit()
    {
        return $this->unit;
    }
    
    /**
     * Returns the enddate
     *
     * @return \DateTime $enddate
     */
    public function getEnddate()
    {
        return $this->enddate;
    }
    
    /**
     * Sets the enddate
     *
     * @param \DateTime $enddate
     * @return void
     */
    public function setEnddate(\DateTime $enddate)
    {
        $this->enddate = $enddate;
    }
    
    /**
     * Returns the difficulty
     *
     * @return string $difficulty
     */
    public function getDifficulty()
    {
        return $this->difficulty;
    }
    
    /**
     * Sets the difficulty
     *
     * @param string $difficulty
     * @return void
     */
    public function setDifficulty($difficulty)
    {
        $this->difficulty = $difficulty;
    }
    
    public function getDifficultyCode()
    {
        return substr($this->difficulty,strpos($this->difficulty,'-')+1);
    }
    /**
     * Returns the description
     *
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }
    
    /**
     * Sets the whiterisk
     *
     * @param string $whiterisk
     * @return void
     */
    public function setWhiterisk($whiterisk)
    {
        $this->whiterisk = $whiterisk;
    }
    
    /**
     * Returns the whiterisk
     *
     * @return string $whiterisk
     */
    public function getWhiterisk()
    {
        return $this->whiterisk;
    }
    
    /**
     * Sets the description
     *
     * @param string $description
     * @return void
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }
    
    /**
     * Returns the requirements
     *
     * @return string $requirements
     */
    public function getRequirements()
    {
        return $this->requirements;
    }
    
    /**
     * Sets the requirements
     *
     * @param string $requirements
     * @return void
     */
    public function setRequirements($requirements)
    {
        $this->requirements = $requirements;
    }
    
    /**
     * Returns the equipment
     *
     * @return string $equipment
     */
    public function getEquipment()
    {
        return $this->equipment;
    }
    
    /**
     * Sets the equipment
     *
     * @param string $equipment
     * @return void
     */
    public function setEquipment($equipment)
    {
        $this->equipment = $equipment;
    }
    
 
    
    /**
     * Returns the meeting
     *
     * @return string $meeting
     */
    public function getMeeting()
    {
        return $this->meeting;
    }
    
    /**
     * Sets the meeting
     *
     * @param int $meeting
     * @return void
     */
    public function setMeeting($meeting)
    {
        $this->meeting = $meeting;
    }
    
    
    /**
     * Returns the returningtime
     *
     * @return string $returningtime
     */
    public function getReturningtime()
    {
        return $this->returningtime;
    }
    
    /**
     * Sets the returningtime
     *
     * @param string $returningtime
     * @return void
     */
    public function setReturningtime($returningtime)
    {
        $this->returningtime = $returningtime;
    }
    
    /**
     * Returns the signin
     *
     * @return string $signin
     */
    public function getSignin()
    {
        return $this->signin;
    }
    
    /**
     * Sets the signin
     *
     * @param string $signin
     * @return void
     */
    public function setSignin($signin)
    {
        $this->signin = $signin;
    }
    
    /**
     * Returns the info
     *
     * @return string $info
     */
    public function getInfo()
    {
        return $this->info;
    }
    
    /**
     * Sets the info
     *
     * @param string $info
     * @return void
     */
    public function setInfo($info)
    {
        $this->info = $info;
    }
    
    /**
     * Returns the cost
     *
     * @return string $cost
     */
    public function getCost()
    {
        return $this->cost;
    }
    
    /**
     * Sets the cost
     *
     * @param string $cost
     * @return void
     */
    public function setCost($cost)
    {
        $this->cost = $cost;
    }
    
    /**
     * Returns the special
     *
     * @return string $special
     */
    public function getSpecial()
    {
        return $this->special;
    }
    
    /**
     * Sets the special
     *
     * @param string $special
     * @return void
     */
    public function setSpecial($special)
    {
        $this->special = $special;
    }
    
    /**
     * Returns the state
     *
     * @return int $state
     */
    public function getState()
    {
        return $this->state;
    }
    
    /**
     * Sets the state
     *
     * @param int $state
     * @return void
     */
    public function setState($state)
    {
        $this->state = $state;
    }
    
    /**
     * Returns the tourenleiter
     *
     * @return \BeatHeim\HibTourenplanung\Domain\Model\Tourenleiter $tourenleiter
     */
    public function getTourenleiter()
    {
        return $this->tourenleiter;
    }
    
    /**
     * Sets the tourenleiter
     *
     * @param \BeatHeim\HibTourenplanung\Domain\Model\Tourenleiter $tourenleiter
     * @return void
     */
    public function setTourenleiter(\BeatHeim\HibTourenplanung\Domain\Model\Tourenleiter $tourenleiter)
    {
        $this->tourenleiter = $tourenleiter;
    }
    
    /**
     * Returns the eregister
     *
     * @return bool $eregister
     */
    public function getEregister()
    {
        return $this->eregister;
    }
    
    /**
     * Sets the eregister
     *
     * @param bool $eregister
     * @return void
     */
    public function setEregister($eregister)
    {
        $this->eregister = $eregister;
    }
    
    /**
     * Returns the boolean state of eregister
     *
     * @return bool
     */
    public function isEregister()
    {
        return $this->eregister;
    }
    
    /**
     * Adds a Participant
     *
     * @param \BeatHeim\HibTourenplanung\Domain\Model\Participant $subscriber
     * @TYPO3\CMS\Extbase\Annotation\IgnoreValidation("subscriber")
     * @return void
     */
    public function addSubscriber(\BeatHeim\HibTourenplanung\Domain\Model\Participant $subscriber)
    {
        $this->subscriber->attach($subscriber);
    }
    
    /**
     * Removes a Participant
     *
     * @param \BeatHeim\HibTourenplanung\Domain\Model\Participant $subscriberToRemove The Participant to be removed
     * @return void
     */
    public function removeSubscriber(\BeatHeim\HibTourenplanung\Domain\Model\Participant $subscriberToRemove)
    {
        $this->subscriber->detach($subscriberToRemove);
    }
    
    /**
     * Returns the subscriber
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\BeatHeim\HibTourenplanung\Domain\Model\Participant> $subscriber
     */
    public function getSubscriber()
    {
        return $this->subscriber;
    }
    
    /**
     * Sets the subscriber
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\BeatHeim\HibTourenplanung\Domain\Model\Participant> $subscriber
     * @return void
     */
    public function setSubscriber(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $subscriber)
    {
        $this->subscriber = $subscriber;
    }
    
    /**
     * Returns the picture
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $picture
     */
    public function getPicture()
    {
        return $this->picture;
    }
    
    /**
     * Sets the picture
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $picture
     * @return void
     */
    public function setPicture(\TYPO3\CMS\Extbase\Domain\Model\FileReference $picture)
    {
        $this->picture = $picture;
    }
    
    
     /**
     * Returns true if fotos found
     *
     * @return string 
     */
    public function getFotos()
    {
	$foto = "";
	$uid = $this ->getUid();
	$objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
	$repos = $objectManager -> get('BeatHeim\HibTourenplanung\Domain\Repository\ReportRepository');

	$report = $repos->findAlwaysByTour($uid);
	//\TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($report);
	if ($report -> count()){
	    
	    $Nr = $report[0]->getPicture()->count();
	    if ($Nr > 0)
		$foto = $report[0]->getUid();
	}
	return $foto;
    }

    /**
     * Returns the report uid
     *
     * @return int $reportUid
     */
    public function getReport()
    {
	$uid = $this ->getUid();
	$objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
	$repos = $objectManager -> get('BeatHeim\HibTourenplanung\Domain\Repository\ReportRepository');
	//$repos = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('BeatHeim\HibTourenplanung\Domain\Repository\ReportRepository');
	$report = $repos->findByTour($uid);
	//\TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($report);
	if ($report -> count()){
	    return $report[0];
	}
    }
    
     /**
     * Returns the report uid (even if not executed/visible)
     *
     * @return int $reportUid
     */
    public function getReportAll()
    {
	$uid = $this ->getUid();
	$objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
	$repos = $objectManager -> get('BeatHeim\HibTourenplanung\Domain\Repository\ReportRepository');
	//$repos = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('BeatHeim\HibTourenplanung\Domain\Repository\ReportRepository');
	$report = $repos->findAlwaysByTour($uid);
	//\TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($report);
	if ($report -> count()){
	    return $report[0];
	}
    }

}