<?php
namespace BeatHeim\HibTourenplanung\Domain\Model;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016 Beat Heim <betzgi@gmail.com>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Member
 */
class Member extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * partner_uid
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\BeatHeim\HibTourenplanung\Domain\Model\Member>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $partner_uid = null;
    
    /**
     * leader_uid
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\BeatHeim\HibTourenplanung\Domain\Model\Tourenleiter>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $leader_uid = null;
    
    /**
     * name
     *
     * @var string
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     */
    protected $name = '';
    
    /**
     * firstname
     *
     * @var string
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     */
    protected $firstname = '';
    
    /**
     * sex
     *
     * @var string
     */
    protected $sex = '';
    
    /**
     * paying
     *
     * @var int
     */
    protected $paying = 0;
    
    /**
     * email
     *
     * @var string
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     * @TYPO3\CMS\Extbase\Annotation\Validate("EmailAddressValidator")
     */
    protected $email = '';
    
    /**
     * phone
     *
     * @var string
     */
    protected $phone = '';
  
      
    /**
     * address
     *
     * @var string
     */
    protected $address = '';
    
    /**
     * message
     *
     * @var string
     */
    protected $comment = '';
    
     /**
     * birthdate
     *
     * @var \DateTime
     * @TYPO3\CMS\Extbase\Annotation\Validate("\BeatHeim\HibTourenplanung\Validation\Validator\StartdateValidator")
     */
    protected $birthdate = null;
    
     /**
     * memberdate
     *
     * @var \DateTime
     * @TYPO3\CMS\Extbase\Annotation\Validate("\BeatHeim\HibTourenplanung\Validation\Validator\StartdateValidator")
     */
    protected $memberdate = null;
    
    /**
     * __construct
     */
    public function __construct()
    {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }
    
    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        
    }
    
    /**
     * Returns the name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Sets the name
     *
     * @param string $name
     * @return void
     */
    public function setName($name)
    {
        $this->name = $name;
    }
    
    /**
     * Returns the firstname
     *
     * @return string $firstname
     */
    public function getFirstname()
    {
        return $this->firstname;
    }
    
    /**
     * Sets the firstname
     *
     * @param string $firstname
     * @return void
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }
    
    /**
     * Returns the sex
     *
     * @return string $sex
     */
    public function getSex()
    {
        return $this->sex;
    }
    
    /**
     * Sets the sex
     *
     * @param string $sex
     * @return void
     */
    public function setSex($sex)
    {
        $this->sex = $sex;
    }
    
    /**
     * Returns the birthdate
     *
     * @return \DateTime $birthdate
     */
    public function getBirthdate()
    {
        return $this->birthdate;
    }
    
    /**
     * Sets the birthdate
     *
     * @param \DateTime $birthdate
     * @return void
     */
    public function setBirthdate(\DateTime $birthdate)
    {
        $this->birthdate = $birthdate;
    }
    /**
     * Returns the memberdate
     *
     * @return \DateTime $memberdate
     */
    public function getMemberdate()
    {
        return $this->memberdate;
    }
    
    /**
     * Sets the memberdate
     *
     * @param \DateTime $memberdate
     * @return void
     */
    public function setMemberdate(\DateTime $memberdate)
    {
        $this->memberdate = $memberdate;
    }
    
    /**
     * Returns the paying
     *
     * @return int $paying
     */
    public function getPaying()
    {
        return $this->paying;
    }
    
    /**
     * Sets the paying
     *
     * @param int $paying
     * @return void
     */
    public function setPaying($paying)
    {
        $this->paying = $paying;
    }
    
    /**
     * Adds a Tourenleiter
     *
     * @param \BeatHeim\HibTourenplanung\Domain\Model\Tourenleiter $tourenleiter
     * @return void
     */
    public function addTourenleiter(\BeatHeim\HibTourenplanung\Domain\Model\Tourenleiter $tourenleiter)
    {
        $this->tourenleiter->attach($tourenleiter);
    }
    
    /**
     * Removes a Tourenleiter
     *
     * @param \BeatHeim\HibTourenplanung\Domain\Model\Tourenleiter $tourenleiterToRemove The Tourenleiter to be removed
     * @return void
     */
    public function removeTourenleiter(\BeatHeim\HibTourenplanung\Domain\Model\Tourenleiter $tourenleiterToRemove)
    {
        $this->tourenleiter->detach($tourenleiterToRemove);
    }
    
     /**
     * Returns the Leader_uid
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\BeatHeim\HibTourenplanung\Domain\Model\Tourenleiter> $learder_uid
     */
    public function getLeader_uid()
    {
        return $this->leader_uid;
    }
    
    /**
     * Sets the Leader_uid
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\BeatHeim\HibTourenplanung\Domain\Model\Tourenleiter> $leader_uid
     * @return void
     */
    public function setLeader_uid(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $leader)
    {
        $this->leader_uid = $leader;
    }
    
    /**
     * Adds a Partner_uid
     *
     * @param \BeatHeim\HibTourenplanung\Domain\Model\Member $member
     * @return void
     */
    public function addPartner_uid(\BeatHeim\HibTourenplanung\Domain\Model\Member $member)
    {
        $this->partner_uid->attach($member);
    }
    
    /**
     * Removes a Partner_uid
     *
     * @param \BeatHeim\HibTourenplanung\Domain\Model\Member $memberToRemove The Member to be removed
     * @return void
     */
    public function removePartner_uid(\BeatHeim\HibTourenplanung\Domain\Model\Member $memberToRemove)
    {
        $this->partner_uid->detach($memberToRemove);
    }
    
    /**
     * Returns the partner_uid
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\BeatHeim\HibTourenplanung\Domain\Model\Member> $member
     */
    public function getPartner_uid()
    {
        return $this->partner_uid;
    }
    
    /**
     * Sets the partner_uid
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\BeatHeim\HibTourenplanung\Domain\Model\Member> $member
     * @return void
     */
    public function setPartner_uid(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $member)
    {
        $this->partner_uid = $member;
    }
    
    /**
     * Returns the email
     *
     * @return string $email
     */
    public function getEmail()
    {
        return $this->email;
    }
    
    /**
     * Sets the email
     *
     * @param string $email
     * @return void
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * Returns the address
     *
     * @return string $address
     */
    public function getAddress()
    {
        return $this->address;
    }
    
    /**
     * Sets the address
     *
     * @param string $address
     * @return void
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }
    
    /**
     * Returns the phone
     *
     * @return string $phone
     */
    public function getPhone()
    {
        return $this->phone;
    }
    
    /**
     * Sets the phone
     *
     * @param string $phone
     * @return void
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }
    
    /**
     * Returns the comment
     *
     * @return string $comment
     */
    public function getComment()
    {
        return $this->comment;
    }
    
    /**
     * Sets the comment
     *
     * @param string $comment
     * @return void
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }
    
}