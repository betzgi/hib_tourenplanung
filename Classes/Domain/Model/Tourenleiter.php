<?php
namespace BeatHeim\HibTourenplanung\Domain\Model;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016 Beat Heim <betzgi@gmail.com>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Tourenleiter
 */
class Tourenleiter extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
    * inactive 
    * 
    * @var bool 
    */
    protected $inactive = FALSE;

    /**
     * name
     *
     * @var string
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     */
    protected $name = '';
    
    /**
     * email
     *
     * @var string
     */
    protected $email = '';
    
    /**
     * address
     *
     * @var string
     */
    protected $address = '';
    
    /**
     * phone
     *
     * @var string
     */
    protected $phone = '';
    
    /**
     * emergency
     *
     * @var string
     */
    protected $emergency = '';
    
    /**
     * picture
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     */
    protected $picture = null;
    
    
     /**
     * type
     *
     * @var string
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     */
    protected $type = '';
    
     /**
     * youth
     *
     * @var bool
     */
    protected $youth = false;
    
     /**
     * snow
     *
     * @var bool
     */
    protected $snow = false;
    
    /**
     * feuser
     *
     * @var int
     */
    protected $feuser = 0;
    
     /**
     * function
     *
     * @var string
     */
    protected $function = '';
    
    /** 
    * Returns inactive 
    * 
    * @return boolean $inactive 
    */
    public function getInactive() {
	return $this->inactive;
    }

    /** 
    * Returns the boolean state of inactive 
    * 
    * @return boolean 
    */
    public function isInactive() {    
	return $this->getInactive();
    }

    /**
    * @param boolean $inactive
    * @return void
    */
    public function setInactive($hidden) {
       $this->inactive = $inactive;
    }
  
    /**
     * Returns the function
     *
     * @return string $function
     */
    public function getFunction()
    {
        return $this->function;
    }
    
    /**
     * Sets the function
     *
     * @param string $function
     * @return void
     */
    public function setFunction($function)
    {
        $this->function = $function;
    }
    
    /**
     * Returns the type
     *
     * @return string $type
     */
    public function getType()
    {
        return $this->type;
    }
    
    /**
     * Sets the type
     *
     * @param string $type
     * @return void
     */
    public function setType($type)
    {
        $this->type = $type;
    }
    /**
     * Returns the youth
     *
     * @return bool $youth
     */
    public function getYouth()
    {
        return $this->youth;
    }
    
     /**
     * Returns the youth
     *
     * @return bool $youth
     */
    public function isYouth()
    {
        return $this->youth;
    }
    
    /**
     * Sets the youth
     *
     * @param bool $youth
     * @return void
     */
    public function setYouth($youth)
    {
        $this->youth = $youth;
    }
    
    /**
     * Returns the snow
     *
     * @return bool $snow
     */
    public function isSnow()
    {
        return $this->snow;
    }
    
    /**
     * Sets the snow
     *
     * @param bool $snow
     * @return void
     */
    public function setSnow($snow)
    {
        $this->snow = $snow;
    }
    
    /**
     * Returns the feuser
     *
     * @return int $feuser
     */
    public function getFeuser()
    {
        return $this->feuser;
    }
    
    /**
     * Sets the feuser
     *
     * @param int $feuser
     * @return void
     */
    public function setFeuser($feuser)
    {
        $this->feuser = $feuser;
    }
    
    /**
     * Returns the name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Sets the name
     *
     * @param string $name
     * @return void
     */
    public function setName($name)
    {
        $this->name = $name;
    }
    
    /**
     * Returns the email
     *
     * @return string $email
     */
    public function getEmail()
    {
        return $this->email;
    }
    
    /**
     * Sets the email
     *
     * @param string $email
     * @return void
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }
    
    /**
     * Returns the picture
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $picture
     */
    public function getPicture()
    {
        return $this->picture;
    }
    
    /**
     * Sets the picture
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $picture
     * @return void
     */
    public function setPicture(\TYPO3\CMS\Extbase\Domain\Model\FileReference $picture)
    {
        $this->picture = $picture;
    }
    
    /**
     * Returns the address
     *
     * @return string $address
     */
    public function getAddress()
    {
        return $this->address;
    }
    
    /**
     * Sets the address
     *
     * @param string $address
     * @return void
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }
    
    /**
     * Returns the phone
     *
     * @return string $phone
     */
    public function getPhone()
    {
        return $this->phone;
    }
    
    /**
     * Sets the phone
     *
     * @param string $phone
     * @return void
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }
    
    /**
     * Returns the emergency
     *
     * @return string $emergency
     */
    public function getEmergency()
    {
        return $this->emergency;
    }
    
    /**
     * Sets the emergency
     *
     * @param string $emergency
     * @return void
     */
    public function setEmergency($emergency)
    {
        $this->emergency = $emergency;
    }

}