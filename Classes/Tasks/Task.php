<?php
namespace BeatHeim\HibTourenplanung\Tasks;

class Task extends \TYPO3\CMS\Scheduler\Task\AbstractTask
{

    public function execute() {
	$objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
	$tourRepository = $objectManager->get('BeatHeim\HibTourenplanung\Domain\Repository\TourRepository');
	$touren = $tourRepository->findOneYear();
	$subject = "BL: Bitte Tourenbericht erfassen";
	//\TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($touren);	
	foreach ($touren as $tour)
	{
	    $email = $tour->getTourenleiter()->getEmail();
	    
	    //$email = "webmaster@bergfreunde.ch";
	    
	    $tourenleiter = $tour->getTourenleiter()->getName();
	    $text = " Hallo ".$tourenleiter."\n \nBitte erfasse doch den Tourenbericht für diese Tour noch: \n";
	    $text .= $tour->getTitle()." vom ".$tour->getStartdate()->format('d-m-Y')." \n \n";
	    $year = $tour->getStartdate()->format('Y');
	    $link = "https://www.bergfreunde.ch/intern/touren?tx_hibtourenplanung_tourlist[year]=".$year."#collapse".$tour->getUid();

	    $text .= "Link zur Tour: ".$link;

	    $this->sendEmail($email,$tourenleiter,$subject, $text);

	}
        $successfullyExecuted = true;
        return $successfullyExecuted;
    }
    
    protected function sendEmail($mailTo, $nameTo, $subject, $emailBody)
    {
	$mailFrom = 'tourenchef@bergfreunde.ch'; 
	$nameFrom = "Tourenchef BL";
	$mailBCC = 'webmaster@bergfreunde.ch';
	$nameBCC = 'Webmaster BL';
	$message = (new \TYPO3\CMS\Core\Mail\MailMessage())
	    ->setFrom(array($mailFrom => $nameFrom))
	    ->setTo(array($mailTo => $nameTo))
	    ->setBcc(array($mailBCC => $nameBCC))
	    ->setSubject($subject)
	    ->setBody($emailBody);
	//\TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($message);
	$message->send();
	return $message->isSent();
    }
}