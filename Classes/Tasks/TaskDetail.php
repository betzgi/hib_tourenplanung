<?php
namespace BeatHeim\HibTourenplanung\Tasks;

class TaskDetail extends \TYPO3\CMS\Scheduler\Task\AbstractTask
{

    public function execute() {
	$objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
	$tourRepository = $objectManager->get('BeatHeim\HibTourenplanung\Domain\Repository\TourRepository');
	$touren = $tourRepository->findDetailRemind();
	$subject = "BL: Bitte Details zu Tourausschreibung erfassen";
	//\TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($touren);	
	foreach ($touren as $tour)
	{
	    $email = $tour->getTourenleiter()->getEmail();
	    
	    //$email = "webmaster@bergfreunde.ch";
	    
	    $tourenleiter = $tour->getTourenleiter()->getName();
	    $text = " Hallo ".$tourenleiter."\n \nIn 4 Monaten (am ".$tour->getStartdate()->format('d-m-Y').") ist deine Tour '".$tour->getTitle()."' geplant.  \nBitte erfasse doch die Tourendetails für diese Tour und stelle sicher, dass sie auf sichtbar gestellt ist. \n \n";
	    $year = $tour->getStartdate()->format('Y');
	    $link = "https://www.bergfreunde.ch/intern/touren?tx_hibtourenplanung_tourlist[year]=".$year."#collapse".$tour->getUid();

	    $text .= "Link zur Tour: ".$link;

	    $this->sendEmail($email,$tourenleiter,$subject, $text);

	}
        $successfullyExecuted = true;
        return $successfullyExecuted;
    }
    
    protected function sendEmail($mailTo, $nameTo, $subject, $emailBody)
    {
	$mailFrom = 'tourenchef@bergfreunde.ch'; 
	$nameFrom = "Tourenchef BL";
	$mailBCC = 'webmaster@bergfreunde.ch';
	$nameBCC = 'Webmaster BL';
	$message = (new \TYPO3\CMS\Core\Mail\MailMessage())
	    ->setFrom(array($mailFrom => $nameFrom))
	    ->setTo(array($mailTo => $nameTo))
	    ->setBcc(array($mailBCC => $nameBCC))
	    ->setSubject($subject)
	    ->setBody($emailBody);
	//\TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($message);
	$message->send();
	return $message->isSent();
    }
}