<?php
namespace BeatHeim\HibTourenplanung\Controller;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Database\ConnectionPool;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016 Beat Heim <betzgi@gmail.com>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * ReportController
 */
class ReportController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * reportRepository
     *
     * @var \BeatHeim\HibTourenplanung\Domain\Repository\ReportRepository
     * @TYPO3\CMS\Extbase\Annotation\Inject`
     */
    protected $reportRepository = NULL;
    
    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {
	if ($this->request->hasArgument('year'))
	{
	    $year = $this->request->getArgument('year');
	}
	else
	{
	    $year = date("Y");
	}
        $this->view->assign('year', $year);
	
	$reports = $this->reportRepository->findByYear($year, $this->settings['unit']);
        $this->view->assign('reports', $reports);
    }
    
    /**
     * action listPrint
     *
     * @return void
     */
    public function listPrintAction()
    {
	if ($this->request->hasArgument('year'))
	{
	    $year = $this->request->getArgument('year');
	}
	else
	{
	    $year = date("Y");
	}
        $this->view->assign('year', $year);
	
	$reports = $this->reportRepository->findByYearExecuted($year, $this->settings['unit']);

        $this->view->assign('reports', $reports);
    }
    
    
    
     /**
     * action listEdit
     *
     * @return void
     */
    public function listEditAction()
    {
	if ($this->request->hasArgument('year'))
	{
	    $year = $this->request->getArgument('year');
	}
	else
	{
	    $year = date("Y");
	}
        $this->view->assign('year', $year);	
	$reports = $this->reportRepository->findAllByYear($year);
	//\TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($reports);
        $this->view->assign('reports', $reports);
    }
    
     /**
     * action listPhoto
     *
     * @return void
     */
    public function listPhotoAction()
    {
	if ($this->request->hasArgument('year'))
	{
	    $year = $this->request->getArgument('year');
	}
	else
	{
	    $year = date("Y");
	}
        $this->view->assign('year', $year);
	
	$reports = $this->reportRepository->findByYearWithPhoto($year, $this->settings['unit']);
	//\TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($reports);
        $this->view->assign('reports', $reports);
    }
    
    /**
     * action show
     *
     * @param \BeatHeim\HibTourenplanung\Domain\Model\Report $report
     * @TYPO3\CMS\Extbase\Annotation\IgnoreValidation("report")
     * @return void
     */
    public function showAction(\BeatHeim\HibTourenplanung\Domain\Model\Report $report)
    {
	if ($this->request->hasArgument('backPage'))
	{
	    $backPage = $this->request->getArgument('backPage');
	    $this->view->assign('backPage', $backPage);
	}
	
        $this->view->assign('report', $report);
    }
    
     /**
     * action showForChef
     *
     * @param \BeatHeim\HibTourenplanung\Domain\Model\Report $report
     * @TYPO3\CMS\Extbase\Annotation\IgnoreValidation("report")
     * @return void
     */
    public function showForChefAction(\BeatHeim\HibTourenplanung\Domain\Model\Report $report)
    {
	
        $this->view->assign('report', $report);
    }
    
    /**
     * action new
     * 
     * @param \BeatHeim\HibTourenplanung\Domain\Model\Tour $tour
     * @TYPO3\CMS\Extbase\Annotation\IgnoreValidation("tour")
     * @return void
     */
    public function newAction(\BeatHeim\HibTourenplanung\Domain\Model\Tour $tour = NULL)
    {
        $reports = $this->reportRepository->findAll();
	$tourenMitRep[]= 0;
	foreach ($reports as $report)
	    $tourenMitRep[] = $report->getTour();
	$this->tourRepository = $this->objectManager->get('BeatHeim\HibTourenplanung\Domain\Repository\TourRepository');
	$touren = $this->tourRepository->findAllWithNoReport($tourenMitRep);
	$this->view->assign('tour', $tour);
	$this->view->assign('touren', $touren);
    }
    
    /**
     * action create
     *
     * @param \BeatHeim\HibTourenplanung\Domain\Model\Report $newReport
     * @TYPO3\CMS\Extbase\Annotation\IgnoreValidation("newReport")
     * @return void
     */
    public function createAction(\BeatHeim\HibTourenplanung\Domain\Model\Report $newReport)
    {
	//\TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($newReport);
	$this->addFlashMessage('Der Report wurde erstellt und per Email an Tourenchef versendet'); 
	$newReport->setSent(1);
	
	if ($newReport->getExecuted() == 1)
	{
	    //Participants übernehmen von Tour
	    $leader = new \BeatHeim\HibTourenplanung\Domain\Model\Participant();
	    $leader->setName($newReport->getTour()->getTourenleiter()->getName());
	    $leader->setPid($this->settings['participantPid']);
	    $leader->setLeader(true);
	    $newReport->addParticipant($leader);
	    //Teilnehmer
	    $participants = $newReport->getTour()->getSubscriber();
	    //$this->items = new  \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
	    foreach ($participants as $participant)
	    {
		$newReport->addParticipant($participant);
	    }
	    
	    $this->reportRepository->add($newReport);
	}
	else
	{
	   $this->reportRepository->add($newReport);
	}
	$persistenceManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance("TYPO3\\CMS\\Extbase\\Persistence\\Generic\\PersistenceManager");
	$persistenceManager->persistAll();
	    
       // send Email
	$subject = "BL: Neuer Tourenreport: ".$newReport->getTour()->getTitle().' vom '.$newReport->getTour()->getStartdate()->format('d-m-Y');
	if ($newReport->getTour()->getTourenleiter()->getEmail())
	{
	    $mailFrom = $newReport->getTour()->getTourenleiter()->getEmail();
	    $nameFrom = $newReport->getTour()->getTourenleiter()->getName();
	}
	else
	{
	    $mailFrom = "webmaster@bergfreunde.ch";
	    $nameFrom = "BL Webmaster"; 
	}     
	$mailTo = $this->settings['tourenchefEmail'];

	//$mailTo = "webmaster@bergfreunde.ch";

	$mailCC = "webmaster@bergfreunde.ch";
	$nameCC = "BL Webmaster"; 
	$nameTo = "Tourenchef";
	$emailBody = $newReport->getTour()->getTourenleiter()->getName()." hat einen neuen Tourenreport erfasst:";
	$emailBody .= "\n \nTour: ".$newReport->getTour()->getTitle()." vom ".$newReport->getTour()->getStartdate()->format('d-m-Y');
	if ($newReport->getExecuted() == 1) {
	    $emailBody .= " \n \nDurchführung: durchgeführt";
	} else {
	    $emailBody .= " \n \nDurchführung: nicht durchgeführt";
	}
	$emailBody .= " \n \nKurzbericht: \n".$newReport->getShorttext();
	$emailBody .= " \n \nTeilnehmer: ".count($newReport->getParticipant());
	$emailBody .= " \n \nBericht für Webseite: \n".$newReport->getText();


	$uriBuilder = $this->controllerContext->getUriBuilder();
	$uriBuilder->reset();
	$uriBuilder->setTargetPageUid($this->settings['reportEditPage']);
	$year = $newReport->getTour()->getStartdate()->format('Y');
	$uriBuilder->setArguments(array(
	    'tx_hibtourenplanung_tourlist' => array(
	    'year' => $year,
	    'action' => 'showForChef',
	    'report' => $newReport->getUid()
	    )
	));
	$link = $this->request->getBaseUri().$uriBuilder->build();

	$emailBody .= " \n \nLink zum Tourenreport: ".$link;
	$this->sendEmail($mailTo, $nameTo, $mailCC, $nameCC, $mailFrom, $nameFrom, $subject, $emailBody);
	$this->cacheService->clearPageCache();
	$this->redirect('listEdit');

    }
    
    /**
     * action edit
     *
     * @param \BeatHeim\HibTourenplanung\Domain\Model\Report $report
     * @TYPO3\CMS\Extbase\Annotation\IgnoreValidation("report")
     * @TYPO3\CMS\Extbase\Annotation\IgnoreValidation("participant")
     * @TYPO3\CMS\Extbase\Annotation\IgnoreValidation("touren")

     * @return void
     */
    public function editAction(\BeatHeim\HibTourenplanung\Domain\Model\Report $report)
    {
        $this->view->assign('report', $report);
	
	$reports = $this->reportRepository->findAll();
	
	foreach ($reports as $report)
	    $tourenMitRep[] = $report->getTour();
	$this->tourRepository = $this->objectManager->get('BeatHeim\HibTourenplanung\Domain\Repository\TourRepository');
	$touren = $this->tourRepository->findAllWithNoReport($tourenMitRep);
	$this->view->assign('touren', $touren);
    }
    
    /**
     * action update
     * @TYPO3\CMS\Extbase\Annotation\IgnoreValidation("participant")
     * @TYPO3\CMS\Extbase\Annotation\IgnoreValidation("report")
     * @param \BeatHeim\HibTourenplanung\Domain\Model\Report $report
     * @return void
     */
    public function updateAction(\BeatHeim\HibTourenplanung\Domain\Model\Report $report)
    {
        $this->reportRepository->update($report);
	
	$uriBuilder = $this->controllerContext->getUriBuilder();
        $uriBuilder->reset();
	// specify the page ID for the link
	
	$uriBuilder->setTargetPageUid($this->settings['reportEditPage']);
	$year = $report->getTour()->getStartdate()->format('Y');
	$uriBuilder->setArguments(array(
		'tx_hibtourenplanung_tourlist' => array(
		'year' => $year)));
	$uriBuilder->setSection('collapse'.$report->getUid());
	$uri = $uriBuilder->build();
	$this->cacheService->clearPageCache();
	$this->redirectToUri($uri);
        //$this->redirect('listEdit');
    }
    
     /**
     * action delete
     *
     * @param \BeatHeim\HibTourenplanung\Domain\Model\Report $report
     * @return void
     * @TYPO3\CMS\Extbase\Annotation\IgnoreValidation("report")
     */
    public function deleteAction(\BeatHeim\HibTourenplanung\Domain\Model\Report $report)
    {
        $this->view->assign('report', $report);
    }
    
    /**
     * action deleteNow
     *
     * @param \BeatHeim\HibTourenplanung\Domain\Model\Report $report
     * @return void
     * @TYPO3\CMS\Extbase\Annotation\IgnoreValidation("report")
     */
    public function deleteNowAction(\BeatHeim\HibTourenplanung\Domain\Model\Report $report)
    {
        $this->addFlashMessage('Tourenbericht wurde gelöscht.');
        $this->reportRepository->remove($report);
	$this->cacheService->clearPageCache();
        $this->redirect('listEdit');
    }

     /**
     * action toggleState
     *
     * @param \BeatHeim\HibTourenplanung\Domain\Model\Report $report
     * @TYPO3\CMS\Extbase\Annotation\IgnoreValidation("report")
     * @return void
     */
    public function toggleStateAction(\BeatHeim\HibTourenplanung\Domain\Model\Report $report)
    {
        if ($report->getState()==0)
	{
	    $report->setState(1);
	}
	else
	{
	    $report->setState(0);
	}
	$this->reportRepository->update($report);
	$this->cacheService->clearPageCache();
	
	$uriBuilder = $this->controllerContext->getUriBuilder();
        $uriBuilder->reset();
	// specify the page ID for the link
	$uriBuilder->setTargetPageUid($this->settings['reportEditPage']);
	$year = $report->getTour()->getStartdate()->format('Y');
	$uriBuilder->setArguments(array(
		'tx_hibtourenplanung_tourlist' => array(
		'year' => $year)));
	$uriBuilder->setSection('collapse'.$report->getUid());
	$uri = $uriBuilder->build();
	$this->redirectToUri($uri);
	//$this->redirect('listEdit');
    }
    
     /**
     * action uploadPhoto
     *
     * @param \BeatHeim\HibTourenplanung\Domain\Model\Report $report
     * @TYPO3\CMS\Extbase\Annotation\IgnoreValidation("report")
     * @return void
     */
    public function uploadPhotoAction(\BeatHeim\HibTourenplanung\Domain\Model\Report $report)
    {
	//\TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($_FILES);
	$i = 0;
	$files = $_FILES['file'];
	$temp = $_FILES['file']['tmp_name'];
	$newFolder = preg_replace ( '/[^a-z0-9-_]/i', '', $report->getTour()->getStartdate()->format('Y-m-d').'_'.$report->getTour()->getTitle());
	$newImagePath = '/fotos/report/'.$newFolder; 
	if (!file_exists('fileadmin'.$newImagePath)) {
	    mkdir('fileadmin'.$newImagePath);
	}
	foreach($files['name'] as $name)
	//\TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($newImagePath);
	{     
	    $tmpFile = $temp[$i];

	    $storageRepository = $this->objectManager->get('TYPO3\\CMS\\Core\\Resource\\StorageRepository');
	    $storage = $storageRepository->findByUid('1'); //this is the fileadmin storage
	    //\TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($storage);
	    //build the new storage folder
	    $targetFolder = $storage->getFolder($newImagePath);
	    //$targetFolder = 'fileadmin/fotos/report/xx';
	    //file name, be shure that this is unique
	    $newFileName = $tmpName;
	    //build sys_file
	    $movedNewFile = $storage->addFile($tmpFile, $targetFolder, $name);
	    //\TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($movedNewFile);
	    $this->objectManager->get('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\PersistenceManager')->persistAll();
	    $storagePid = $report->getPid();
	    //now we build the file reference
	    $values = array(
		'pid' => $storagePid,
		'uid_local'=> $movedNewFile->getUid(),
		'uid_foreign'=> $report->getUid(),
		'tablenames' => 'tx_hibtourenplanung_domain_model_report',
		'fieldname' => 'picture',
		);

	    $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
	    $databaseConnectionForRef = $connectionPool->getConnectionForTable('sys_file_reference');
	    $databaseConnectionForRef->insert('sys_file_reference', $values);
	    
	    $i++;
	}
	$this->cacheService->clearPageCache();
	$uriBuilder = $this->controllerContext->getUriBuilder();
        $uriBuilder->reset();

	// send Email to Webmaster
	$subject = "BL: Neue Fotos wurden hochgeladen: ".$report->getTour()->getTitle().' vom '.$report->getTour()->getStartdate()->format('d-m-Y');
	$mailFrom = "webmaster@bergfreunde.ch";
	$nameFrom = "BL Webmaster";   
	$mailTo = "webmaster@bergfreunde.ch";
	$nameTo = "BL Webmaster"; 
	$emailBody = $report->getTour()->getTourenleiter()->getName()." hat neue Fotos hochgeladen:";
	$emailBody .= "\n \nTour: ".$report->getTour()->getTitle()." vom ".$report->getTour()->getStartdate()->format('d-m-Y');

	$uriBuilder->reset();
	$uriBuilder->setTargetPageUid($this->settings['reportEditPage']);
	$year = $report->getTour()->getStartdate()->format('Y');
	$uriBuilder->setArguments(array(
	    'tx_hibtourenplanung_tourlist' => array(
	    'year' => $year,
	    'action' => 'showForChef',
	    'report' => $report->getUid()
	    )
	));
	$link = $this->request->getBaseUri().$uriBuilder->build();

	$emailBody .= " \n \nLink zum Tourenreport: ".$link;
	$this->sendEmail($mailTo, $nameTo, $mailCC, $nameCC, $mailFrom, $nameFrom, $subject, $emailBody);
	
        // specify the page ID for the link
	$uriBuilder->setTargetPageUid($this->settings['reportEditPage']);
	$uriBuilder->setArguments(array(
		'tx_hibtourenplanung_tourlist' => array(
		'year' => $year)));
	$uriBuilder->setSection('collapse'.$report->getUid());
	$uri = $uriBuilder->build();   
	$this->cacheService->clearPageCache();
	$this->redirectToUri($uri);
    }
    
     /**
     * action uploadPhoto
     *
     * @param \BeatHeim\HibTourenplanung\Domain\Model\Report $report
     * @TYPO3\CMS\Extbase\Annotation\IgnoreValidation("report")
     * @return void
     */
    public function newPhotoAction(\BeatHeim\HibTourenplanung\Domain\Model\Report $report)
    {
	$this->view->assign('report', $report);
    }
    
     /**
     * action deletePhoto (only Ref)
     *
     * @param \BeatHeim\HibTourenplanung\Domain\Model\Report $report
     * @param int $picture
     * @TYPO3\CMS\Extbase\Annotation\IgnoreValidation("report")
     * @return void
     */
    public function deletePhotoAction(\BeatHeim\HibTourenplanung\Domain\Model\Report $report, $picture)
    {
	//\TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($picture);
	//\TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($report);
	
	$resourceFactory = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Resource\\ResourceFactory');
	$fileReferenceObject = $resourceFactory->getFileReferenceObject($picture);
	//\TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($fileReferenceObject);
	//\TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($fileReferenceObject->getUid());
	$values = array(
		'uid' => $fileReferenceObject->getUid()
		);
	$connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
	$databaseConnectionForRef = $connectionPool->getConnectionForTable('sys_file_reference');
	$databaseConnectionForRef->delete('sys_file_reference', $values);

	//$fileWasDeleted = $fileReferenceObject->remove();
	//$fileWasDeleted = $fileReferenceObject->getOriginalFile()->delete();
	$this->cacheService->clearPageCache();

	$uriBuilder = $this->controllerContext->getUriBuilder();
        $uriBuilder->reset();
	// specify the page ID for the link
	$uriBuilder->setTargetPageUid($this->settings['reportEditPage']);
	$year = $report->getTour()->getStartdate()->format('Y');
	$uriBuilder->setArguments(array(
		'tx_hibtourenplanung_tourlist' => array(
		'year' => $year)));
	$uriBuilder->setSection('collapse'.$report->getUid());
	$uri = $uriBuilder->build();
	$this->redirectToUri($uri);
    }
    
     /**
     * Build relations for FAL
     * 
     * @param int    $newStorageUid //The UID of the  model
     * @param array  $file //The file model of the image
     * @param string $field //the name of the relation field
     * @param string $table //the table of the model
     */
    private function buildRelations($newStorageUid, $file, $field, $table, $storagePid) {

        $data = array();
        $data['sys_file_reference']['NEW1234'] = array(
            'uid_local' => $file->getUid(),
            'uid_foreign' => $newStorageUid, // uid of your content record or own model
            'tablenames' => $table, //tca table name
            'fieldname' => $field, //see tca for fieldname
            'pid' => $storagePid,
            'table_local' => 'sys_file',
        );
        $data[$table][$newStorageUid] = array('image' => 'NEW1234'); //this is needed, i dont know why :( but not stored in tables

        /** @var \TYPO3\CMS\Core\DataHandling\DataHandler $tce */
        $tce = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Core\DataHandling\DataHandler'); // create TCE instance
        $tce->start($data, array());
        $tce->process_datamap();
    }
    
      	/**
	 * This is the main-function for sending Mails
	 *
	 * @param string $mailTo
	 * @param string $nameTo
	 * @param string $mailCC
	 * @param string $nameCC
	 * @param string $mailFrom
	 * @param string $nameFrom
	 * @param string $subject
	 * @param string $emailBody
	 *
	 * @return boolean
	 */
    protected function sendEmail($mailTo, $nameTo, $mailCC="", $nameCC, $mailFrom ="", $nameFrom="", $subject, $emailBody)
    {
	//$mailTo = $this->settings['tourenchefEmail'];
	if ($mailFrom =="")
	{
	    $mailFrom = $this->settings['absenderEmail'];
	    $nameFrom = "Bergfreunde Luzern";
	}
	if ($mailCC =="") {
	   $message = (new \TYPO3\CMS\Core\Mail\MailMessage())
	    ->setFrom(array($mailFrom => $nameFrom))
	    ->setTo(array($mailTo => $nameTo))
	    ->setSubject($subject)
	    ->setBody($emailBody); 
	}
	else {
	   $message = (new \TYPO3\CMS\Core\Mail\MailMessage())
	    ->setFrom(array($mailFrom => $nameFrom))
	    ->setTo(array($mailTo => $nameTo))
	    ->setCc(array($mailCC => $nameCC))
	    ->setSubject($subject)
	    ->setBody($emailBody); 
	}
	//\TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($message);
	$message->send();
	return $message->isSent();
    }
}