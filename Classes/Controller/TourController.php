<?php
namespace BeatHeim\HibTourenplanung\Controller;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016 Beat Heim <betzgi@gmail.com>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * TourController
 */
class TourController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * tourRepository
     *
     * @var \BeatHeim\HibTourenplanung\Domain\Repository\TourRepository
     * @TYPO3\CMS\Extbase\Annotation\Inject`
     */
    protected $tourRepository = NULL;
    
    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {
        if ($this->request->hasArgument('year'))
	{
	    $year = $this->request->getArgument('year');
	}
	else
	{
	    $year = date("Y");
	}
        $this->view->assign('year', $year);
	$tours = $this->tourRepository->findByYear($year,$this->settings['unit']);
        $this->view->assign('tours', $tours);
    }
    
     /**
     * action listPrint
     *
     * @return void
     */
    public function listPrintAction()
    {
        if ($this->request->hasArgument('year'))
	{
	    $year = $this->request->getArgument('year');
	}
	else
	{
	    $year = date("Y");
	}
        $this->view->assign('year', $year);
	$tours = $this->tourRepository->findByYear($year,1);
	$toursJ = $this->tourRepository->findByYear($year,2);
        $this->view->assign('tours', $tours);
	$this->view->assign('toursJ', $toursJ);
    }
    
     /**
     * action emailParticipant
     *
     * @return void
     */
    public function emailParticipantAction()
    {
        if ($this->request->hasArgument('tour'))
	{
	    $tour = $this->request->getArgument('tour');
	}
	$tour = $this->tourRepository->findByUid($tour);
        $this->view->assign('tour', $tour);
    }
    
    /**
     * action listPrintProv
     *
     * @return void
     */
    public function listPrintProvAction()
    {
        if ($this->request->hasArgument('year'))
	{
	    $year = $this->request->getArgument('year');
	}
	else
	{
	    $year = date("Y");
	}
        $this->view->assign('year', $year);
	$tours = $this->tourRepository->findByYearProv($year,1);
	$toursJ = $this->tourRepository->findByYearProv($year,2);
        $this->view->assign('tours', $tours);
	$this->view->assign('toursJ', $toursJ);
    }
    
    /**
     * action listPrintParticipant
     *
     * @return void
     */
    public function listPrintParticipantAction()
    {
        if ($this->request->hasArgument('tour'))
	{
	    $tour = $this->request->getArgument('tour');
	}
	$tour = $this->tourRepository->findByUid($tour);
        $this->view->assign('tour', $tour);
    }
     /**
     * action listLatest
     *
     * @return void
     */
    public function listLatestAction() {

	$tours = $this->tourRepository->findLatest($this->settings['unit']);

	$this->view->assign('tours', $tours);
	//\TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($this->settings['unit']);
	$news = $this->getNews();

	$this->view->assign('news', $news);
    }

    /**
     * action listMobile
     *
     * @return void
     */
    public function listMobileAction() {

	$tours = $this->tourRepository->findLatestMobile($this->settings['unit']);
	$this->view->assign('tours', $tours);

	$news = $this->getNews();

	$this->view->assign('news', $news);
    }

    /**
     * action listEdit
     *
     * @return void
     */
    public function listEditAction()
    {
        if ($this->request->hasArgument('year'))
	{
	    $year = $this->request->getArgument('year');
	}
	else
	{
	    $year = date("Y");
	}
        $this->view->assign('year', $year);
	
	$tours = $this->tourRepository->findByYearInactive($year,$this->settings['unit']);
        $this->view->assign('tours', $tours);
    }
    
    /**
     * action show
     *
     * @param \BeatHeim\HibTourenplanung\Domain\Model\Tour $tour
     * @return void
     * @TYPO3\CMS\Extbase\Annotation\IgnoreValidation("tour")
     */
    public function showAction(\BeatHeim\HibTourenplanung\Domain\Model\Tour $tour)
    {
        $this->view->assign('tour', $tour);
    }
    
    /**
     * action delete
     *
     * @param \BeatHeim\HibTourenplanung\Domain\Model\Tour $tour
     * @return void
     * @TYPO3\CMS\Extbase\Annotation\IgnoreValidation("tour")
     */
    public function deleteAction(\BeatHeim\HibTourenplanung\Domain\Model\Tour $tour)
    {
        $this->view->assign('tour', $tour);
    }
    
     /**
     * action deleteNow
     *
     * @param \BeatHeim\HibTourenplanung\Domain\Model\Tour $tour
     * @return void
     * @TYPO3\CMS\Extbase\Annotation\IgnoreValidation("tour")
     */
    public function deleteNowAction(\BeatHeim\HibTourenplanung\Domain\Model\Tour $tour)
    {
        $this->addFlashMessage('Tour wurde gelöscht.');
        $this->tourRepository->remove($tour);
        $this->redirect('listEdit');
    }
    /**
     * action new
     *
     * @return void
     */
    public function newAction()
    {
        $this->view->assign('types', $this->getTypes());
	$this->view->assign('difficulties', $this->getDifficulties());
	$this->view->assign('units', $this->getUnits());
	$this->tourenleiterRepository = $this->objectManager->get('BeatHeim\HibTourenplanung\Domain\Repository\TourenleiterRepository');
	$tourenleiter = $this->tourenleiterRepository->findAllActive();
	$this->view->assign('tourenleiter', $tourenleiter);
	
	$login = $this->tourenleiterRepository->findLogTourenleiter();
	$this->view->assign('login', $login); 
    }
    
    /**
     * action create
     *
     * @param \BeatHeim\HibTourenplanung\Domain\Model\Tour $newTour
     * @return void
     */
    public function createAction(\BeatHeim\HibTourenplanung\Domain\Model\Tour $newTour)
    {
        $this->addFlashMessage('neue Tour wurde gespeichert und per Email an Tourenchef gemeldet');
        $this->tourRepository->add($newTour);
	// BILD
	$newImagePath = 'fotos/touren';

	if ($_FILES['tx_hibtourenplanung_tourlist']['name']['picture'][0]) {

	    //be careful - you should validate the file type! This is not included here       
	    $tmpName = $_FILES['tx_hibtourenplanung_tourlist']['name']['picture'][0];
	    $tmpFile  = $_FILES['tx_hibtourenplanung_tourlist']['tmp_name']['picture'][0];

	    $storageRepository = $this->objectManager->get('TYPO3\\CMS\\Core\\Resource\\StorageRepository');
	    $storage = $storageRepository->findByUid('1'); //this is the fileadmin storage
	    //build the new storage folder
	    $targetFolder = $storage->getFolder($newImagePath);
	    //$targetFolder = 'fileadmin/fotos/touren';
	    //file name, be shure that this is unique
	    $newFileName = $tmpName;

	    //build sys_file
	    $movedNewFile = $storage->addFile($tmpFile, $targetFolder, $newFileName);
	    $this->objectManager->get('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\PersistenceManager')->persistAll();
	    $storagePid = $newTour->getPid();
	    //now we build the file reference
	    //see private function anotiations!
	    $this->buildRelations($newTour->getUid(), $movedNewFile, 'picture', 'tx_hibtourenplanung_domain_model_tour', $storagePid);
	}
	
	// notify email an Tourenchef
	$subject = "BL: Neue Tourenausschreibung: ".$newTour->getTitle().' am '.$newTour->getStartdate()->format('d-m-Y');
	if ($newTour->getTourenleiter()->getEmail())
	{
	    $mailFrom[$newTour->getTourenleiter()->getEmail()] = $newTour->getTourenleiter()->getName();
	}
	else
	{ 
	    $mailFrom["webmaster@bergfreunde.ch"] = "BL Webmaster";
	} 
	$mailTo[$this->settings['tourenchefEmail']] = "Tourenchef";
	$mailCC["webmaster@bergfreunde.ch"] = "BL Webmaster";

	$emailBody = $newTour->getTourenleiter()->getName()." hat eine neue Tour erfasst:";
	$emailBody .= "\n \nTour: ".$newTour->getTitle()." ".$newTour->getType()." am ".$newTour->getStartdate()->format('d-m-Y');
	$emailBody .= " \n \nBeschreibung: \n".$newTour->getDescription();

	$persistenceManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance("TYPO3\\CMS\\Extbase\\Persistence\\Generic\\PersistenceManager");
	$persistenceManager->persistAll();
	
	$uriBuilder = $this->controllerContext->getUriBuilder();
	$uriBuilder->reset();
	$uriBuilder->setTargetPageUid($this->settings['tourEditPage']);
	$year = $newTour->getStartdate()->format('Y');
	$uriBuilder->setArguments(array(
		'tx_hibtourenplanung_tourlist' => array(
		'year' => $year,
		'action' => 'show',
		'tour' => $newTour->getUid()
		    
	    )
	));
	$link = $this->request->getBaseUri().$uriBuilder->build();
	    
	$emailBody .= " \n \nLink zur Tour: ".$link;
	
	$this->sendEmail($mailTo, $mailCC, $mailFrom, $subject, $emailBody);
	$this->cacheService->clearPageCache();
        $this->redirect('listEdit');
    }
    
        /**
     * action sendEmailParticipant
     *
     * @param \BeatHeim\HibTourenplanung\Domain\Model\Tour $tour
     * @TYPO3\CMS\Extbase\Annotation\IgnoreValidation("tour")
     * @return void
     */
    public function sendEmailParticipantAction(\BeatHeim\HibTourenplanung\Domain\Model\Tour $tour)
    {
	// send mail to all
	$subject = $tour->getTitle();
	$emailBody = $tour->getDescription();
	$mailCC[$tour->getTourenleiter()->getEmail()] = $tour->getTourenleiter()->getName();

	
	if ($tour->getTourenleiter()->getEmail())
	{
	    $mailFrom[$tour->getTourenleiter()->getEmail()] = $tour->getTourenleiter()->getName();
	}
	else
	{
	    $mailFrom["webmaster@bergfreunde.ch"] = "BL Webmaster";
	} 
	$mailTo = array();
	foreach ($tour->getSubscriber() as $subscriber)
	{
	    if ($subscriber->getEmail()<>''){
		$mailTo[$subscriber->getEmail()] = $subscriber->getName();
		//$nameTo[] = $subscriber->getName();
	    }
	}
	$this->sendEmail($mailTo, $mailCC, $mailFrom, $subject, $emailBody);
        
	$uriBuilder = $this->controllerContext->getUriBuilder();
        $uriBuilder->reset();
	// specify the page ID for the link
	$uriBuilder->setTargetPageUid($this->settings['tourEditPage']);
	$year = $tour->getStartdate()->format('Y');
	$uriBuilder->setArguments(array(
		'tx_hibtourenplanung_tourlist' => array(
		'year' => $year,
		'action' => 'listEdit')));
	$uriBuilder->setSection('collapse'.$tour->getUid());
	$uri = $uriBuilder->build();
	\TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($mailTo);
	$this->redirectToUri($uri);
    }
    
    /**
     * action edit
     *
     * @param \BeatHeim\HibTourenplanung\Domain\Model\Tour $tour
     * @TYPO3\CMS\Extbase\Annotation\IgnoreValidation("tour")
     * @return void
     */
    public function editAction(\BeatHeim\HibTourenplanung\Domain\Model\Tour $tour)
    {
	//\TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($tour);
        $this->view->assign('tour', $tour);
	$this->view->assign('types', $this->getTypes());
	$this->view->assign('difficulties', $this->getDifficulties());
	$this->view->assign('units', $this->getUnits());
	$this->tourenleiterRepository = $this->objectManager->get('BeatHeim\HibTourenplanung\Domain\Repository\TourenleiterRepository');
	$tourenleiter = $this->tourenleiterRepository->findAllActive();
	$this->view->assign('tourenleiter', $tourenleiter);
    }
    
    /**
     * action statistic
     *
     * @return void
     */
    public function statisticAction()
    {
	
        if ($this->request->hasArgument('year'))
	{
	    $year = $this->request->getArgument('year');
	}
	else
	{
	    $year = date("Y");
	}
        $this->view->assign('year', $year);
	$statistic = $this->tourRepository->findStatistic($year);
	//\TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($statistic);
        $this->view->assign('statistic', $statistic);
	$this->view->assign('total', $statistic['Total']);
	$this->view->assign('tourenleiterTotal', $statistic);
    }
    
    /**
     * action update
     *
     * @param \BeatHeim\HibTourenplanung\Domain\Model\Tour $tour
     * @TYPO3\CMS\Extbase\Annotation\IgnoreValidation("tour")
     * @TYPO3\CMS\Extbase\Annotation\IgnoreValidation("subscriber")
     * @return void
     */
    public function updateAction(\BeatHeim\HibTourenplanung\Domain\Model\Tour $tour)
    {
      $updateText = "";
      if ($tour->_getCleanProperty("title") != $tour->getTitle())
      {
	  $updateText .= " \nTitel vorher: ".$tour->_getCleanProperty("title").", Titel neu: ".$tour->getTitle();
      }
      if ($tour->_getCleanProperty("startdate") != $tour->getStartdate())
      {
	  $updateText .= " \nDatum vorher: ".$tour->_getCleanProperty("startdate")->format('d-m-Y').", Datum neu: ".$tour->getStartdate()->format('d-m-Y');
      }
      if ($updateText != "")
      {
	  $this->addFlashMessage('Tour wurde geändert und per Email an Tourenchef gemeldet');
	  //notify email an Tourenchef
	  $subject = "BL: Tourenausschreibung geändert: ".$tour->getTitle().' am '.$tour->getStartdate()->format('d-m-Y');
	if ($tour->getTourenleiter()->getEmail())
	{
	    $mailFrom[$tour->getTourenleiter()->getEmail()] = $tour->getTourenleiter()->getName();
	}
	else
	{
	    $mailFrom["webmaster@bergfreunde.ch"] = "BL Webmaster";
	} 
	$mailTo[$this->settings['tourenchefEmail']] = "Tourenschef";
	
        //$mailTo["webmaster@bergfreunde.ch"] = "Tourenschef";
	
	$mailCC["webmaster@bergfreunde.ch"] = "BL Webmaster";

	$emailBody = $tour->getTourenleiter()->getName()." hat eine Tour geändert:";
	$emailBody .= "\n \nTour: ".$tour->getTitle()." ".$tour->getType()." am ".$tour->getStartdate()->format('d-m-Y');
	$emailBody .= " \n \nfolgende Änderungen wurden gemacht: \n" ;
	$emailBody .= $updateText;
	
	$uriBuilder = $this->controllerContext->getUriBuilder();
	$uriBuilder->reset();
	$uriBuilder->setTargetPageUid($this->settings['tourEditPage']);
	$year = $tour->getStartdate()->format('Y');
	$uriBuilder->setArguments(array(
		'tx_hibtourenplanung_tourlist' => array(
		'year' => $year,
		'action' => 'show',
		'tour' => $tour->getUid()
	    )
	));
	$link = $this->request->getBaseUri().$uriBuilder->build();
	    
	$emailBody .= " \n \nLink zur Tour: ".$link;
	
	$this->sendEmail($mailTo, $mailCC, $mailFrom, $subject, $emailBody);	  
      }
      $this->tourRepository->update($tour);
	//\TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($this->arguments);
	// BILD
	$newImagePath = 'fotos/touren';

	if ($_FILES['tx_hibtourenplanung_tourlist']['name']['picture'][0]) {
	    if ($tour->getPicture() != NULL)
	    {
		//remove old file first
		$resourceFactory = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Resource\\ResourceFactory');
		$fileReferenceObject = $resourceFactory->getFileReferenceObject($tour->getPicture()->getUid());
		$fileWasDeleted = $fileReferenceObject->getOriginalFile()->delete();
	    }

	    //be careful - you should validate the file type! This is not included here       
	    $tmpName = $_FILES['tx_hibtourenplanung_tourlist']['name']['picture'][0];
	    $tmpFile  = $_FILES['tx_hibtourenplanung_tourlist']['tmp_name']['picture'][0];

	    $storageRepository = $this->objectManager->get('TYPO3\\CMS\\Core\\Resource\\StorageRepository');
	    $storage = $storageRepository->findByUid('1'); //this is the fileadmin storage
	    //build the new storage folder
	    $targetFolder = $storage->getFolder($newImagePath);
	    //$targetFolder = 'fileadmin/fotos/touren';
	    //file name, be shure that this is unique
	    $newFileName = $tmpName;

	    //build sys_file
	    $movedNewFile = $storage->addFile($tmpFile, $targetFolder, $newFileName);
	    $this->objectManager->get('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\PersistenceManager')->persistAll();
	    $storagePid = $tour->getPid();
	    //now we build the file reference
	    //see private function anotiations!
	    $this->buildRelations($tour->getUid(), $movedNewFile, 'picture', 'tx_hibtourenplanung_domain_model_tour', $storagePid);
	}
	
	$uriBuilder = $this->controllerContext->getUriBuilder();
        $uriBuilder->reset();
	// specify the page ID for the link
	$uriBuilder->setTargetPageUid($this->settings['tourEditPage']);
	$year = $tour->getStartdate()->format('Y');
	$uriBuilder->setArguments(array(
		'tx_hibtourenplanung_tourlist' => array(
		'year' => $year)));
	$uriBuilder->setSection('collapse'.$tour->getUid());
	$uri = $uriBuilder->build();
	$this->cacheService->clearPageCache();
	$this->redirectToUri($uri);
    }
    
    
     /**
     * action toggleState
     *
     * @param \BeatHeim\HibTourenplanung\Domain\Model\Tour $tour
     * @TYPO3\CMS\Extbase\Annotation\IgnoreValidation("tour")
     * @TYPO3\CMS\Extbase\Annotation\IgnoreValidation("subscriber")
     * @return void
     */
    public function toggleStateAction(\BeatHeim\HibTourenplanung\Domain\Model\Tour $tour)
    {
        if ($tour->getState()==0)
	{
	    $tour->setState(1);
	}
	else
	{
	    $tour->setState(0);
	}
	$this->tourRepository->update($tour);
	$this->cacheService->clearPageCache();
	$uriBuilder = $this->controllerContext->getUriBuilder();
        $uriBuilder->reset();
	// specify the page ID for the link
	$uriBuilder->setTargetPageUid($this->settings['tourEditPage']);
	$year = $tour->getStartdate()->format('Y');
	$uriBuilder->setArguments(array(
		'tx_hibtourenplanung_tourlist' => array(
		'year' => $year)));
	$uriBuilder->setSection('collapse'.$tour->getUid());
	$uri = $uriBuilder->build();
	$this->redirectToUri($uri);
    }
    
    public function initializeCreateAction() 
    {
	$this->arguments['newTour']
	->getPropertyMappingConfiguration()
	->forProperty('startdate')
	->setTypeConverterOption('TYPO3\\CMS\\Extbase\\Property\\TypeConverter\\DateTimeConverter', \TYPO3\CMS\Extbase\Property\TypeConverter\DateTimeConverter::CONFIGURATION_DATE_FORMAT, 'Y-m-d');
	$this->arguments['newTour']
	->getPropertyMappingConfiguration()
	->forProperty('enddate')
	->setTypeConverterOption('TYPO3\\CMS\\Extbase\\Property\\TypeConverter\\DateTimeConverter', \TYPO3\CMS\Extbase\Property\TypeConverter\DateTimeConverter::CONFIGURATION_DATE_FORMAT, 'Y-m-d');
     }
    
    public function initializeUpdateAction() 
     {
	$this->arguments['tour']
	->getPropertyMappingConfiguration()
	->forProperty('startdate')
	->setTypeConverterOption('TYPO3\\CMS\\Extbase\\Property\\TypeConverter\\DateTimeConverter', \TYPO3\CMS\Extbase\Property\TypeConverter\DateTimeConverter::CONFIGURATION_DATE_FORMAT, 'Y-m-d');
	$this->arguments['tour']
	->getPropertyMappingConfiguration()
	->forProperty('enddate')
	->setTypeConverterOption('TYPO3\\CMS\\Extbase\\Property\\TypeConverter\\DateTimeConverter', \TYPO3\CMS\Extbase\Property\TypeConverter\DateTimeConverter::CONFIGURATION_DATE_FORMAT, 'Y-m-d');
     }
    
    /**
    * prepare types for select box
    *
    * @return array
    */
    public function getTypes() {
	$types = array();
	
	$type = new \stdClass();
	$type->key = '';
	$type->value = 'undefiniert';
	$types[] = $type;
	$type = new \stdClass();
	$type->key = '(S)';
	$type->value = 'Skitour (S)';
	$types[] = $type;
	$type = new \stdClass();
	$type->key = '(S/SB)';
	$type->value = 'Ski/Snowboardtour (S/SB)';
	$types[] = $type;
	$type = new \stdClass();
	$type->key = '(SL)';
	$type->value = 'Schneeschuhtour (SL)';
	$types[] = $type;
	$type = new \stdClass();
	$type->key = '(SHT)';
	$type->value = 'Skihochtour (SHT)';
	$types[] = $type;
	$type = new \stdClass();
	$type->key = '(HT)';
	$type->value = 'Hochtour (HT)';
	$types[] = $type;
	$type = new \stdClass();
	$type->key = '(K)';
	$type->value = 'Klettern (K)';
	$types[] = $type;
	$type = new \stdClass();
	$type->key = '(KS)';
	$type->value = 'Klettrsteig (KS)';
	$types[] = $type;
	$type = new \stdClass();
	$type->key = '(BW)';
	$type->value = 'Bergwanderung (BW)';
	$types[] = $type;
	$type = new \stdClass();
	$type->key = '(W)';
	$type->value = 'Wanderung (W)';
	$types[] = $type;
	$type = new \stdClass();
	$type->key = '(MTB)';
	$type->value = 'Mountainbike (MTB)';
	$types[] = $type;
	$type = new \stdClass();
	$type->key = '(KA)';
	$type->value = 'Kanu (KA)';
	$types[] = $type;
	return $types;
    }

        /**
    * prepare types for select box
    *
    * @return array
    */
    public function getDifficulties2() {
	$difficulties = array();
	
	$difficulty = new \stdClass();
	$difficulty->key = '';
	$difficulty->value = 'undefiniert';
	$difficulties[] = $difficulty;
	$difficulty = new \stdClass();
	$difficulty->key = 'T1';
	$difficulty->value = 'Wanderung: Wandern (T1)';
	$difficulties[] = $difficulty;
	$difficulty = new \stdClass();
	$difficulty->key = 'T2';
	$difficulty->value = 'Wanderung: Bergwandern (T2)';
	$difficulties[] = $difficulty;
	$difficulty = new \stdClass();
	$difficulty->key = 'T3';
	$difficulty->value = 'Wanderung: Bergwandern (T3)';
	$difficulties[] = $difficulty;
	$difficulty = new \stdClass();
	$difficulty->key = 'T4';
	$difficulty->value = 'Wanderung: Alpinwandern (T4)';
	$difficulties[] = $difficulty;
	$difficulty = new \stdClass();
	$difficulty->key = 'T5';
	$difficulty->value = 'Wanderung: anspruchsvolles Alpinwnadern (T5)';
	$difficulties[] = $difficulty;
	$difficulty = new \stdClass();
	$difficulty->key = 'L';
	$difficulty->value = 'Hochgebirgs- & Skitour: leicht (L)';
	$difficulties[] = $difficulty;
	$difficulty = new \stdClass();
	$difficulty->key = 'WS';
	$difficulty->value = 'Hochgebirgs- & Skitour: wenig schwierig (WS)';
	$difficulties[] = $difficulty;
	$difficulty = new \stdClass();
	$difficulty->key = 'S';
	$difficulty->value = 'Hochgebirgs- & Skitour: schwierig (S)';
	$difficulties[] = $difficulty;
	$difficulty = new \stdClass();
	$difficulty->key = 'SS';
	$difficulty->value = 'Hochgebirgs- & Skitour: sehr schwierig (SS)';
	$difficulties[] = $difficulty;
	$difficulty = new \stdClass();
	$difficulty->key = 'WT1';
	$difficulty->value = 'Schneeschuhtouren: leichte Schneeschuhwanderung (WT1)';
	$difficulties[] = $difficulty;
	$difficulty = new \stdClass();
	$difficulty->key = 'WT2';
	$difficulty->value = 'Schneeschuhtouren: Schneeschuhwanderung (WT2)';
	$difficulties[] = $difficulty;
	$difficulty = new \stdClass();
	$difficulty->key = 'WT3';
	$difficulty->value = 'Schneeschuhtouren: anspruchsvolle Schneeschuhwanderung (WT3)';
	$difficulties[] = $difficulty;
	$difficulty = new \stdClass();
	$difficulty->key = 'WT4';
	$difficulty->value = 'Schneeschuhtouren: Schneeschuhtour (WT4)';
	$difficulties[] = $difficulty;
	$difficulty = new \stdClass();
	$difficulty->key = 'WT5';
	$difficulty->value = 'Schneeschuhtouren: alpine Schneeschuhtour (WT5)';
	$difficulties[] = $difficulty;
	$difficulties[] = $difficulty;
	$difficulty = new \stdClass();
	$difficulty->key = 'WT6';
	$difficulty->value = 'Schneeschuhtouren: anspruchsvolle alpine Schneeschuhtour (WT6)';
	$difficulties[] = $difficulty;
	$difficulties[] = $difficulty;
	$difficulty = new \stdClass();
	$difficulty->key = 'KS1';
	$difficulty->value = 'Klettersteige: leicht (KS1)';
	$difficulties[] = $difficulty;
	$difficulty = new \stdClass();
	$difficulty->key = 'KS2';
	$difficulty->value = 'Klettersteige: mittel (KS2)';
	$difficulties[] = $difficulty;
	$difficulty = new \stdClass();
	$difficulty->key = 'KS3';
	$difficulty->value = 'Klettersteige: ziemlich schwierig (KS3)';
	$difficulties[] = $difficulty;
	$difficulty = new \stdClass();
	$difficulty->key = 'KS4';
	$difficulty->value = 'Klettersteige: schwierig (KS4)';
	$difficulties[] = $difficulty;
	$difficulty = new \stdClass();
	$difficulty->key = 'KS5';
	$difficulty->value = 'Klettersteige: sehr schwierig (KS5)';
	$difficulties[] = $difficulty;
	$difficulty = new \stdClass();
	$difficulty->key = 'KS6';
	$difficulty->value = 'Klettersteige: extrem schwierig (KS6)';
	$difficulties[] = $difficulty;
	$difficulty = new \stdClass();
	$difficulty->key = 'S0';
	$difficulty->value = 'Mountainbike: S0';
	$difficulties[] = $difficulty;
	$difficulty = new \stdClass();
	$difficulty->key = 'S1';
	$difficulty->value = 'Mountainbike: S1';
	$difficulties[] = $difficulty;
	$difficulty = new \stdClass();
	$difficulty->key = 'S2';
	$difficulty->value = 'Mountainbike: S2';
	$difficulties[] = $difficulty;
	$difficulty = new \stdClass();
	$difficulty->key = 'S3';
	$difficulty->value = 'Mountainbike: S3';
	$difficulties[] = $difficulty;
	return $difficulties;
    }
    
    /*****
    * prepare types for select box
    *
    * @return array
    */
    public function getDifficulties() {
	$difficulties = array();
	
	$difficulty = new \stdClass();
	$difficulty->key = '';
	$difficulty->value = 'undefiniert';
	$difficulties[] = $difficulty;
	$difficulty = new \stdClass();
	$difficulty->key = '(W)-T1';
	$difficulty->value = 'Wandern (T1)';
	$difficulties[] = $difficulty;
	$difficulty = new \stdClass();
	$difficulty->key = '(BW)-T2';
	$difficulty->value = 'Bergwandern (T2)';
	$difficulties[] = $difficulty;
	$difficulty = new \stdClass();
	$difficulty->key = '(BW)-T3';
	$difficulty->value = 'Bergwandern (T3)';
	$difficulties[] = $difficulty;
	$difficulty = new \stdClass();
	$difficulty->key = '(BW)-T4';
	$difficulty->value = 'Alpinwandern (T4)';
	$difficulties[] = $difficulty;
	$difficulty = new \stdClass();
	$difficulty->key = '(BW)-T5';
	$difficulty->value = 'Anspruchsvolles Alpinwandern (T5)';
	$difficulties[] = $difficulty;
	
	$difficulty = new \stdClass();
	$difficulty->key = '(S)-L';
	$difficulty->value = 'Skitour: leicht (L)';
	$difficulties[] = $difficulty;
	$difficulty = new \stdClass();
	$difficulty->key = '(S)-WS';
	$difficulty->value = 'Skitour: wenig schwierig (WS)';
	$difficulties[] = $difficulty;
	$difficulty = new \stdClass();
	$difficulty->key = '(S)-ZS';
	$difficulty->value = 'Skitour: ziemlich schwierig (ZS)';
	$difficulties[] = $difficulty;
	$difficulty = new \stdClass();
	$difficulty->key = '(S)-S';
	$difficulty->value = 'Skitour: schwierig (S)';
	$difficulties[] = $difficulty;
	$difficulty = new \stdClass();
	$difficulty->key = '(S/SB)-L';
	$difficulty->value = 'Ski/Snowboardtour: leicht (L)';
	$difficulties[] = $difficulty;
	
	$difficulty = new \stdClass();
	$difficulty->key = '(S/SB)-WS';
	$difficulty->value = 'Ski/Snowboardtour: wenig schwierig (WS)';
	$difficulties[] = $difficulty;
	$difficulty = new \stdClass();
	$difficulty->key = '(S/SB)-ZS';
	$difficulty->value = 'Ski/Snowboardtour: ziemlich schwierig (ZS)';
	$difficulties[] = $difficulty;
	$difficulty = new \stdClass();
	$difficulty->key = '(S/SB)-S';
	$difficulty->value = 'Ski/Snowboardtour: schwierig (S)';
	$difficulties[] = $difficulty;
	
	$difficulty = new \stdClass();
	$difficulty->key = '(SL)-WT1';
	$difficulty->value = 'Schneeschuhtour: leichte Schneeschuhwanderung (WT1)';
	$difficulties[] = $difficulty;
	$difficulty = new \stdClass();
	$difficulty->key = '(SL)-WT2';
	$difficulty->value = 'Schneeschuhtour: Schneeschuhwanderung (WT2)';
	$difficulties[] = $difficulty;
	$difficulty = new \stdClass();
	$difficulty->key = '(SL)-WT3';
	$difficulty->value = 'Schneeschuhtour: anspruchsvolle Schneeschuhwanderung (WT3)';
	$difficulties[] = $difficulty;
	$difficulty = new \stdClass();
	$difficulty->key = '(SL)-WT4';
	$difficulty->value = 'Schneeschuhtour: Schneeschuhtour (WT4)';
	$difficulties[] = $difficulty;
	$difficulty = new \stdClass();
	$difficulty->key = '(SL)-WT5';
	$difficulty->value = 'Schneeschuhtour: alpine Schneeschuhtour (WT5)';
	$difficulties[] = $difficulty;

	$difficulty = new \stdClass();
	$difficulty->key = '(HT)-L';
	$difficulty->value = 'Hochtour: leicht (L)';
	$difficulties[] = $difficulty;
	$difficulty = new \stdClass();
	$difficulty->key = '(HT)-WS';
	$difficulty->value = 'Hochtour: wenig schwierig (WS)';
	$difficulties[] = $difficulty;
	$difficulty = new \stdClass();
	$difficulty->key = '(HT)-ZS';
	$difficulty->value = 'Hochtour: ziemlich schwierig (ZS)';
	$difficulties[] = $difficulty;
	$difficulty = new \stdClass();
	$difficulty->key = '(HT)-S';
	$difficulty->value = 'Hochtour: schwierig (S)';
	$difficulties[] = $difficulty;
	
	
	$difficulties[] = $difficulty;
	$difficulty = new \stdClass();
	$difficulty->key = '(KS)-KS1';
	$difficulty->value = 'Klettersteig: leicht (KS1)';
	$difficulties[] = $difficulty;
	$difficulty = new \stdClass();
	$difficulty->key = '(KS)-KS2';
	$difficulty->value = 'Klettersteig: mittel (KS2)';
	$difficulties[] = $difficulty;
	$difficulty = new \stdClass();
	$difficulty->key = '(KS)-KS3';
	$difficulty->value = 'Klettersteig: ziemlich schwierig (KS3)';
	$difficulties[] = $difficulty;
	$difficulty = new \stdClass();
	$difficulty->key = '(KS)-KS4';
	$difficulty->value = 'Klettersteig: schwierig (KS4)';
	$difficulties[] = $difficulty;

	$difficulty = new \stdClass();
	$difficulty->key = '(MTB)-S0';
	$difficulty->value = 'Mountainbike: S0';
	$difficulties[] = $difficulty;
	$difficulty = new \stdClass();
	$difficulty->key = '(MTB)-S1';
	$difficulty->value = 'Mountainbike: S1';
	$difficulties[] = $difficulty;
	$difficulty = new \stdClass();
	$difficulty->key = '(MTB)-S2';
	$difficulty->value = 'Mountainbike: S2';
	$difficulties[] = $difficulty;
	$difficulty = new \stdClass();
	$difficulty->key = '(MTB)-S3';
	$difficulty->value = 'Mountainbike: S3';
	$difficulties[] = $difficulty;
	
	$difficulty = new \stdClass();
	$difficulty->key = '(K)-';
	$difficulty->value = 'Klettern';
	$difficulties[] = $difficulty;
	
	$difficulty = new \stdClass();
	$difficulty->key = '(KA)-';
	$difficulty->value = 'Kanu';
	$difficulties[] = $difficulty;
	return $difficulties;
    }
    
    /**
    * prepare units for select box
    *
    * @return array
    */
    public function getUnits() {
	$units = array();
	
	$unit = new \stdClass();
	$unit->key = '1';
	$unit->value = 'Bergfreunde';
	$units[] = $unit;
	$unit = new \stdClass();
	$unit->key = '2';
	$unit->value = 'Jugendgruppe';
	$units[] = $unit;
	$unit = new \stdClass();
	$unit->key = '3';
	$unit->value = 'Bergfreunde & Jugendgruppe';
	$units[] = $unit;
	return $units;
    }
    
     /**
     * Build relations for FAL
     * 
     * @param int    $newStorageUid //The UID of the  model
     * @param array  $file //The file model of the image
     * @param string $field //the name of the relation field
     * @param string $table //the table of the model
     */
    private function buildRelations($newStorageUid, $file, $field, $table, $storagePid) {

        $data = array();
        $data['sys_file_reference']['NEW1234'] = array(
            'uid_local' => $file->getUid(),
            'uid_foreign' => $newStorageUid, // uid of your content record or own model
            'tablenames' => $table, //tca table name
            'fieldname' => $field, //see tca for fieldname
            'pid' => $storagePid,
            'table_local' => 'sys_file',
        );
        $data[$table][$newStorageUid] = array('image' => 'NEW1234'); //this is needed, i dont know why :( but not stored in tables

        /** @var \TYPO3\CMS\Core\DataHandling\DataHandler $tce */
        $tce = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Core\DataHandling\DataHandler'); // create TCE instance
        $tce->start($data, array());
        $tce->process_datamap();
    }
    
	/**
	 * This is the main-function for sending Mails
	 *
	 * @param string $mailTo
	 * @param string $nameTo
	 * @param string $mailCC
	 * @param string $nameCC
	 * @param string $mailFrom
	 * @param string $nameFrom
	 * @param string $subject
	 * @param string $emailBody
	 *
	 * @return boolean
	 */
    protected function sendEmail($mailTo, $mailCC, $mailFrom ="", $subject, $emailBody)
    {
	//$mailTo = $this->settings['tourenchefEmail'];
	if (! isset($mailFrom))
	{
	    $mailFrom[$this->settings['absenderEmail']] = "Bergfreunde Luzern";
	}
	$message = (new \TYPO3\CMS\Core\Mail\MailMessage())
	    ->setFrom($mailFrom)
	    ->setTo($mailTo)
	    ->setCc($mailCC)
	    ->setSubject($subject)
	    ->setBody($emailBody);
	//\TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($message);
	$message->send();
	return $message->isSent();
    }
    
    protected function sendMultiEmail($mailTo, $mailCC, $nameCC, $mailFrom ="", $nameFrom="", $subject, $emailBody)
    {
	//$mailTo = $this->settings['tourenchefEmail'];
	if ($mailFrom =="")
	{
	    $mailFrom = $this->settings['absenderEmail'];
	    $nameFrom = "Bergfreunde Luzern";
	}
	$message = (new \TYPO3\CMS\Core\Mail\MailMessage())
	    ->setFrom(array($mailFrom => $nameFrom))
	    ->setTo($mailTo)
	    ->setCc(array($mailCC => $nameCC))
	    ->setSubject($subject)
	    ->setBody($emailBody);
	//\TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($message);
	$message->send();
	return $message->isSent();
    }
    
    protected function getNews()
    {
	$this->reportRepository = $this->objectManager->get('BeatHeim\HibTourenplanung\Domain\Repository\ReportRepository');
	$reports = $this->reportRepository->findLatest($this->settings['unit']);

	$objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager');
	$newsRepository = $objectManager->get('GeorgRinger\\News\\Domain\\Repository\\NewsRepository');
	$uriBuilder = $this->controllerContext->getUriBuilder();
	$newsAll = array();
	for ($i=1; $i < 200; $i++ )   //liest nur die ersten 200 News !!!
	{
	    $got = $newsRepository->findByUid($i);
	    if ($got && $got->getPid() == $this->settings['newsPid'])
		$news[] = $got; 
	}

	usort($news, function($a, $b){
	    return $a->getDateTime() < $b->getDateTime();
	});

	$maxNews = count($news);
	$maxReport = count($reports);
	$iReport = 0;
	$iNews = 0;
	$outNews = array();
	if ($maxNews + $maxReport < 5)
	    $maxItems = $maxNews + $maxReport;
	else
	    $maxItems = 5;
	for ($j=1; $j < $maxItems; $j++ )  {     /// 5 NEWS
	    if($iNews<$maxNews)
		$new = $news[$iNews];
	        $report = $reports[$iReport];
	    
	    if (($iNews==$maxNews) || ($report->getTour()->getStartdate() > $new->getDateTime())) {
		$year = $report->getTour()->getStartdate()->format('Y');
		$neu = array();
		$neu["date"] = $report->getTour()->getStartdate();
		$uriBuilder->reset();
		if ($report->getText() == "") {
		    $neu["text"] = "neue Fotos: " . $report->getTour()->getTitle();
		    $uriBuilder->setTargetPageUid($this->settings['fotoPage']);
		    $uriBuilder->setArguments(array('tx_hibtourenplanung_tourlist' => array('year' => $year, 'controller' => 'Report', 'action' => 'listPhoto', 'report' => $report->getUid())));
		    $uriBuilder->setSection('collapse' . $report->getUid());
		} else {
		    if ($report->getTitle() == '')
			$neu["text"] = "neuer Bericht: " . $report->getTour()->getTitle();
		    else
			$neu["text"] = "neuer Bericht: " . $report->getTitle();
		    $uriBuilder->setTargetPageUid($this->settings['reportPage']);
		    $uriBuilder->setArguments(array('tx_hibtourenplanung_tourlist' => array('year' => $year, 'controller' => 'Report', 'action' => 'show', 'report' => $report->getUid())));
		}
		$neu["uri"] = $uriBuilder->build();
		$outNews[] = $neu;
		$iReport ++;
	    } else {
		$neu = array();
		$neu["date"] = $new->getDateTime();
		$neu["text"] = $new->getTitle();
		$uriBuilder->reset();
		$uriBuilder->setTargetPageUid($this->settings['newsPage']);
		$uriBuilder->setArguments(array('tx_news_pi1' => array('controller' => 'News', 'action' => 'detail', 'news' => $new->getUid())));
		$neu["uri"] = $uriBuilder->build();
		$outNews[] = $neu;
		if (iNews < $maxNews)
		    $iNews ++;
	    }
	}

	// nur 5 nehmen und umsortieren
	$i = count($outNews) - 1;
	$j = 1;
	$out = array();
	while ($j <= $maxItems && $i >= 0) {
	    $out[] = $outNews[$i];
	    $i--;
	    $j++;
	    	}
	//\TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($out);
	return $outNews;
    }
 
}