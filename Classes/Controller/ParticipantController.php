<?php
namespace BeatHeim\HibTourenplanung\Controller;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016 Beat Heim <betzgi@gmail.com>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * ParticipantController
 */
class ParticipantController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * participantRepository
     *
     * @var \BeatHeim\HibTourenplanung\Domain\Repository\ParticipantRepository
     * @TYPO3\CMS\Extbase\Annotation\Inject`
     */
    protected $participantRepository = NULL;
    
    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {
        $participants = $this->participantRepository->findAll();
        $this->view->assign('participants', $participants);
    }
    
    /**
     * action show
     *
     * @param \BeatHeim\HibTourenplanung\Domain\Model\Participant $participant
     * @return void
     */
    public function showAction(\BeatHeim\HibTourenplanung\Domain\Model\Participant $participant)
    {
        $this->view->assign('participant', $participant);
    }
    
    
     /**
     * action newForReport
     *
     * @param \BeatHeim\HibTourenplanung\Domain\Model\Report $report
     * @return void
     * @ignorevalidation $report
     */
    public function newForReportAction(\BeatHeim\HibTourenplanung\Domain\Model\Report $report)
    {
	    $this->view->assign('report', $report);
    }
    
    /**
     * action newForTour
     *
     * @param \BeatHeim\HibTourenplanung\Domain\Model\Tour $tour
     * @return void
     * @ignorevalidation $tour
     */
    public function newForTourAction( \BeatHeim\HibTourenplanung\Domain\Model\Tour $tour)
    {
	    $this->view->assign('tour', $tour);
    }
    
    /**
     * action new
     *
     * @param \BeatHeim\HibTourenplanung\Domain\Model\Tour $tour
     * @return void
     * @ignorevalidation $tour
     */
    public function newAction(\BeatHeim\HibTourenplanung\Domain\Model\Tour $tour)
    {
	$this->view->assign('tour', $tour);
	$login['name'] = $GLOBALS['TSFE']->fe_user->user['name'];
	$login['tel'] = $GLOBALS['TSFE']->fe_user->user['telephone'];
	$login['email'] = $GLOBALS['TSFE']->fe_user->user['email'];
	$login['city'] = $GLOBALS['TSFE']->fe_user->user['city'];
	$login['emergency'] = $GLOBALS['TSFE']->fe_user->user['fax'];
	if (isset($GLOBALS['TSFE']->fe_user->user['uid'])){
	    $login['uid'] = $GLOBALS['TSFE']->fe_user->user['uid'];
	} else {
	    $login['uid'] = '0';
	}
	$this->view->assign('login', $login); 
	//\TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($this->settings);
    }
    
     /**
     * action createForReport
     *
     * @param \BeatHeim\HibTourenplanung\Domain\Model\Report $report
     * @param \BeatHeim\HibTourenplanung\Domain\Model\Participant $newParticipant
     * @ignorevalidation $report
     * @return void
     */
    public function createForReportAction(\BeatHeim\HibTourenplanung\Domain\Model\Report $report, \BeatHeim\HibTourenplanung\Domain\Model\Participant $newParticipant)
    {

	$this->tourRepository = $this->objectManager->get('BeatHeim\HibTourenplanung\Domain\Repository\TourRepository');
	$this->reportRepository = $this->objectManager->get('BeatHeim\HibTourenplanung\Domain\Repository\ReportRepository');
	$newParticipant->setPid($this->settings['participantPid']);
        $this->participantRepository->add($newParticipant);
	//add participant to report
	$report->addParticipant($newParticipant);
	$this->reportRepository->update($report);
        

	// YOUTH ALERT EMAIL
	if ($newParticipant->getYouth() && isset($this->settings['jugendAlertEmail'])){
	    $subject = "BL: JUGENDALARM";
	    $mailTo = $this->settings['jugendAlertEmail'];
	    $nameTo = "Jugendalarm Beauftragter";
	    $mailCC = "webmaster@bergfreunde.ch";
	    $nameCC = "BL Webmaster"; 
	    $emailBody = "Für die Tour ".$report->getTour()->getTitle().' vom '.$report->getTour()->getStartdate()->format('d-m-Y'). " hat sich folgender Jugendlicher angemeldet:";
	    $emailBody .= "\n \nName: ".$newParticipant->getName();
	    $emailBody .= "\n \n Der Webmaster :-) ";
            $this->sendEmail($mailTo, $nameTo, $mailCC, $nameCC, $mailCC, $nameCC, $subject, $emailBody);
	}
	$uriBuilder = $this->controllerContext->getUriBuilder();
        $uriBuilder->reset();
	// specify the page ID for the link

	$uriBuilder->setTargetPageUid($this->settings['reportEditPage']);
	$year = $report->getTour()->getStartdate()->format('Y');
	$uriBuilder->setArguments(array(
		'tx_hibtourenplanung_tourlist' => array(
		'year' => $year)));
	$uriBuilder->setSection('collapse'.$report->getUid());
	$uri = $uriBuilder->build();
	    
	$this->redirectToUri($uri);
	//$this->redirect('list', 'Tour');
    }
    
    /**
     * action createForTour
     *
     * @param \BeatHeim\HibTourenplanung\Domain\Model\Tour $tour
     * @param \BeatHeim\HibTourenplanung\Domain\Model\Participant $newParticipant
     * @ignorevalidation $tour
     * @return void
     */
    public function createForTourAction(\BeatHeim\HibTourenplanung\Domain\Model\Tour $tour, \BeatHeim\HibTourenplanung\Domain\Model\Participant $newParticipant)
    {

	$this->tourRepository = $this->objectManager->get('BeatHeim\HibTourenplanung\Domain\Repository\TourRepository');
	$this->reportRepository = $this->objectManager->get('BeatHeim\HibTourenplanung\Domain\Repository\ReportRepository');
	$newParticipant->setPid($this->settings['participantPid']);
        $this->participantRepository->add($newParticipant);

	//add participant to tour
	$tour->addSubscriber($newParticipant);
	$this->tourRepository->update($tour);

        

	// YOUTH ALERT EMAIL
	if ($newParticipant->getYouth() && isset($this->settings['jugendAlertEmail'])){
	    $subject = "BL: JUGENDALARM";
	    $mailTo = $this->settings['jugendAlertEmail'];
	    $nameTo = "Jugendalarm Beauftragter";
	    $mailCC = "webmaster@bergfreunde.ch";
	    $nameCC = "BL Webmaster"; 
	    $emailBody = "Für die Tour ".$tour->getTitle().' vom '.$tour->getStartdate()->format('d-m-Y'). " hat sich folgender Jugendlicher angemeldet:";
	    $emailBody .= "\n \nName: ".$newParticipant->getName();
	    $emailBody .= "\n \n Der Webmaster :-) ";
            $this->sendEmail($mailTo, $nameTo, $mailCC, $nameCC, $mailCC, $nameCC, $subject, $emailBody);
	}
	
	$uriBuilder = $this->controllerContext->getUriBuilder();
        $uriBuilder->reset();
	// specify the page ID for the link

	$uriBuilder->setTargetPageUid($this->settings['tourEditPage']);
	$year = $tour->getStartdate()->format('Y');
	$uriBuilder->setArguments(array(
		'tx_hibtourenplanung_tourlist' => array(
		'year' => $year)));
	$uriBuilder->setSection('collapse'.$tour->getUid());
	$uri = $uriBuilder->build();
	    
	$this->redirectToUri($uri);
	//$this->redirect('list', 'Tour');
    }
    
    /**
     * action create
     *
     * @param \BeatHeim\HibTourenplanung\Domain\Model\Tour $tour
     * @param \BeatHeim\HibTourenplanung\Domain\Model\Participant $newParticipant
     * @ignorevalidation $tour
     * @return void
     */
    public function createAction(\BeatHeim\HibTourenplanung\Domain\Model\Tour $tour, \BeatHeim\HibTourenplanung\Domain\Model\Participant $newParticipant)
    {
        $this->addFlashMessage('Anmeldung für '. $tour->getTitle() .' wurde erfasst. Du hast ein Bestätigungs-Mail erhalten.');
	//PID
	//\TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($tour);
	//\TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($newParticipant);
	$this->tourRepository = $this->objectManager->get('BeatHeim\HibTourenplanung\Domain\Repository\TourRepository');
	$newParticipant->setPid($this->settings['participantPid']);

        $this->participantRepository->add($newParticipant);
	//add participant to tour
	$tour->addSubscriber($newParticipant);
	$this->tourRepository->update($tour);
	// notify email
	$subject = "BL: Neue Anmeldung für Tour: ".$tour->getTitle().' vom '.$tour->getStartdate()->format('d-m-Y');
	$mailTo = $tour->getTourenleiter()->getEmail();
	$nameTo = $tour->getTourenleiter()->getName();
	$mailCC = $newParticipant->getEmail();
	$nameCC = $newParticipant->getName();
	$emailBody = "Folgender Teilnehmer hat sich angemeldet für die Tour '".$tour->getTitle()."': \n \nName: ".$newParticipant->getName();
	if ($newParticipant->getFeuser()>0)
	    $emailBody .= ' (Registriertes Mitglied)';
	$emailBody .= " \nWohnort: ".$newParticipant->getPlace()." \nTelefon: ".$newParticipant->getPhone()." \nEmail: ".$newParticipant->getEmail()." \nIch habe ein Auto und könnte fahren: ";
	if ($newParticipant->getCar())
	    $emailBody .= "Ja";
	else {
	    $emailBody .= "Nein";
	}
	if ($newParticipant->getYouth())
	    $emailBody .= " \nIch bin unter 20 Jahren: Ja";
	$emailBody.=" \n \nMitteilung: \n".$newParticipant->getMessage();
    $emailBody.=" \n \nDiese Anmeldung gilt als verbindlich. Bei nachträglicher Abmeldung können allfällige Annulationskosten dem Teilnehmer verrechnet werden.";
	$this->sendEmail($mailTo, $nameTo, $mailCC, $nameCC, $mailCC, $nameCC, $subject, $emailBody);
	
	// YOUTH ALERT EMAIL
	if ($newParticipant->getYouth() && isset($this->settings['jugendAlertEmail'])){
	    $subject = "BL: JUGENDALARM";
	    $mailTo = $this->settings['jugendAlertEmail'];
	    $nameTo = "Jugendalarm Beauftragter";
	    $mailCC = "webmaster@bergfreunde.ch";
	    $nameCC = "BL Webmaster"; 
	    $emailBody = "Für die Tour ".$tour->getTitle().' vom '.$tour->getStartdate()->format('d-m-Y'). " hat sich folgender Jugendlicher angemeldet:";
	    $emailBody .= "\n \nName: ".$newParticipant->getName();
	    $emailBody .= "\n \n Der Webmaster :-) ";
            $this->sendEmail($mailTo, $nameTo, $mailCC, $nameCC, $mailCC, $nameCC, $subject, $emailBody);
	}
	
	$uriBuilder = $this->controllerContext->getUriBuilder();
        $uriBuilder->reset();
	// specify the page ID for the link
	$uriBuilder->setTargetPageUid($this->settings['tourPage']);
	$year = $tour->getStartdate()->format('Y');
	$uriBuilder->setArguments(array(
		'tx_hibtourenplanung_tourlist' => array(
		'year' => $year)));
	$uriBuilder->setSection('collapse'.$tour->getUid());
	$uri = $uriBuilder->build();
	$this->redirectToUri($uri);
	//$this->redirect('list', 'Tour');

    }
    
    /**
     * action edit
     *
     * @param \BeatHeim\HibTourenplanung\Domain\Model\Participant $participant
     * @return void
     */
    public function editAction(\BeatHeim\HibTourenplanung\Domain\Model\Participant $participant)
    {
        $this->view->assign('participant', $participant);
    }
    
    /**
     * action update
     *
     * @param \BeatHeim\HibTourenplanung\Domain\Model\Participant $participant
     * @return void
     */
    public function updateAction(\BeatHeim\HibTourenplanung\Domain\Model\Participant $participant)
    {
        $this->addFlashMessage('The object was updated. Please be aware that this action is publicly accessible unless you implement an access check. See http://wiki.typo3.org/T3Doc/Extension_Builder/Using_the_Extension_Builder#1._Model_the_domain', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::ERROR);
        $this->participantRepository->update($participant);
        $this->redirect('list');
    }
    
    /**
     * action deleteForReport
     * @param \BeatHeim\HibTourenplanung\Domain\Model\Report $report
     * @param \BeatHeim\HibTourenplanung\Domain\Model\Participant $participant
     * @ignorevalidation $report
     * @ignorevalidation $participant
     * @return void
     */
    public function deleteForReportAction( \BeatHeim\HibTourenplanung\Domain\Model\Report $report, \BeatHeim\HibTourenplanung\Domain\Model\Participant $participant)
    {
	$participant->setHidden(true);
	$this->participantRepository->update($participant);

	$uriBuilder = $this->controllerContext->getUriBuilder();
	$uriBuilder->reset();
	// specify the page ID for the link
	$uriBuilder->setTargetPageUid($this->settings['reportEditPage']);
	$year = $report->getTour()->getStartdate()->format('Y');
	$uriBuilder->setArguments(array(
		'tx_hibtourenplanung_tourlist' => array(
		'year' => $year)));
	$uriBuilder->setSection('collapse'.$report->getUid());
	$uri = $uriBuilder->build();
	$this->redirectToUri($uri);
	//$this->redirect('listEdit', 'Report' );

    }
    
    /**
     * action deleteForTour
     * @param \BeatHeim\HibTourenplanung\Domain\Model\Tour $tour
     * @param \BeatHeim\HibTourenplanung\Domain\Model\Participant $participant
     * @ignorevalidation $tour
     * @ignorevalidation $participant
     * @return void
     */
    public function deleteForTourAction(\BeatHeim\HibTourenplanung\Domain\Model\Tour $tour, \BeatHeim\HibTourenplanung\Domain\Model\Participant $participant)
    {
	$participant->setHidden(true);
	$this->participantRepository->update($participant);

	$uriBuilder = $this->controllerContext->getUriBuilder();
	$uriBuilder->reset();
	// specify the page ID for the link
	$uriBuilder->setTargetPageUid($this->settings['tourEditPage']);
	$year = $tour->getStartdate()->format('Y');
	$uriBuilder->setArguments(array(
		'tx_hibtourenplanung_tourlist' => array(
		'year' => $year)));
	$uriBuilder->setSection('collapse'.$tour->getUid());
	$uri = $uriBuilder->build();
	$this->redirectToUri($uri);
    }
    
    	/**
	 * This is the main-function for sending Mails
	 *
	 * @param string $mailTo
	 * @param string $nameTo
	 * @param string $mailCC
	 * @param string $nameCC
	 * @param string $mailFrom
	 * @param string $nameFrom
	 * @param string $subject
	 * @param string $emailBody
	 *
	 * @return boolean
	 */
    protected function sendEmail($mailTo, $nameTo, $mailCC, $nameCC, $mailFrom ="", $nameFrom="", $subject, $emailBody)
    {
	//$mailTo = $this->settings['tourenchefEmail'];
	//if ($mailFrom =="")
	//FROM = webmaster; Replyto = real From !!
	    $mailFromG = $this->settings['absenderEmail'];
	    $nameFromG = "Bergfreunde Luzern";
	//}
	$message = (new \TYPO3\CMS\Core\Mail\MailMessage())
	    ->setFrom(array($mailFromG => $nameFromG))
	    ->setReplyTo(array($mailFrom => $nameFrom))
	    ->setTo(array($mailTo => $nameTo))
	    ->setCc(array($mailCC => $nameCC))
	    ->setSubject($subject)
	    ->setBody($emailBody);
	//\TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($message);
	$message->send();
	return $message->isSent();
    }
   
    
}