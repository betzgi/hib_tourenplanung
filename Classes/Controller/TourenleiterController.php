<?php
namespace BeatHeim\HibTourenplanung\Controller;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016 Beat Heim <betzgi@gmail.com>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * TourenleiterController
 */
class TourenleiterController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * tourenleiterRepository
     *
     * @var \BeatHeim\HibTourenplanung\Domain\Repository\TourenleiterRepository
     * @TYPO3\CMS\Extbase\Annotation\Inject`
     */
    protected $tourenleiterRepository = NULL;
    
    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {
	$youth = $this->settings['unit']-1;  // BL = 1(0), JG = 2(1), Snow = 3(2)
	if ($youth == 2)
	{
	    $tourenleiters = $this->tourenleiterRepository->findAllActiveSnow();
	} else {
	    $tourenleiters = $this->tourenleiterRepository->findAllActiveType($youth);
	}
        //\TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($tourenleiters);
        $this->view->assign('tourenleiters', $tourenleiters);
	// touren im aktuellen jahr
	$year = date("Y");
	$this->tourRepository = $this->objectManager->get('BeatHeim\HibTourenplanung\Domain\Repository\TourRepository');
	$tours = $this->tourRepository->findByYear($year,$this->settings['unit']);
	$this->view->assign('tours', $tours);
	$this->view->assign('year', $year);
    }
    
    /**
     * action listVorstand
     *
     * @return void
     */
    public function listVorstandAction()
    {
        $tourenleiters = $this->tourenleiterRepository->findVorstand();
        $this->view->assign('tourenleiters', $tourenleiters);
    }
    
        /**
     * action listEdit
     *
     * @return void
     */
    public function listEditAction()
    {
        $tourenleiters = $this->tourenleiterRepository->findAllForEdit();
	$vorstand = $this->tourenleiterRepository->findVorstand();
        $this->view->assign('tourenleiters', $tourenleiters);
	$this->view->assign('vorstand', $vorstand);
    }
    
    /**
     * action show
     *
     * @param \BeatHeim\HibTourenplanung\Domain\Model\Tourenleiter $tourenleiter
     * @return void
     */
    public function showAction(\BeatHeim\HibTourenplanung\Domain\Model\Tourenleiter $tourenleiter)
    {
	if ($this->request->hasArgument('backTour'))
	{
	    $backPage = $this->request->getArgument('backTour');
	    $this->view->assign('backTour', $backPage);
	}
        $this->view->assign('tourenleiter', $tourenleiter);
	//Touren
	if ($this->request->hasArgument('year'))
	{
	    $year = $this->request->getArgument('year');
	}
	else
	{
	    $year = date("Y");
	}
        $this->view->assign('year', $year);
	$this->tourRepository = $this->objectManager->get('BeatHeim\HibTourenplanung\Domain\Repository\TourRepository');
	$tours = $this->tourRepository->findByTourenleiter($tourenleiter, $year);
	//\TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($tours);
	$this->view->assign('tours', $tours);
    }
    
    /**
     * action new
     *
     * @return void
     */
    public function newAction()
    {
        $this->view->assign('types', $this->getTypes());
    }
    
    /**
     * action create
     *
     * @param \BeatHeim\HibTourenplanung\Domain\Model\Tourenleiter $newTourenleiter
     * @return void
     */
    public function createAction(\BeatHeim\HibTourenplanung\Domain\Model\Tourenleiter $newTourenleiter)
    {
        $this->addFlashMessage('Neuer Tourenleiter angelegt');
        $this->tourenleiterRepository->add($newTourenleiter);
	
	// BILD
	$newImagePath = 'fotos/tourenleiter';

	if ($_FILES['tx_hibtourenplanung_tourlist']['name']['picture'][0]) {
	    //be careful - you should validate the file type! This is not included here       
	    $tmpName = $_FILES['tx_hibtourenplanung_tourlist']['name']['picture'][0];
	    $tmpFile  = $_FILES['tx_hibtourenplanung_tourlist']['tmp_name']['picture'][0];

	    $storageRepository = $this->objectManager->get('TYPO3\\CMS\\Core\\Resource\\StorageRepository');
	    $storage = $storageRepository->findByUid('1'); //this is the fileadmin storage
	    //build the new storage folder
	    $targetFolder = $storage->getFolder($newImagePath);
	    $newFileName = $tmpName;

	    //build sys_file
	    $movedNewFile = $storage->addFile($tmpFile, $targetFolder, $newFileName);
	    $this->objectManager->get('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\PersistenceManager')->persistAll();
	    $storagePid = $newTourenleiter->getPid();
	    //now we build the file reference
	    //see private function anotiations!
	    $this->buildRelations($newTourenleiter->getUid(), $movedNewFile, 'picture', 'tx_hibtourenplanung_domain_model_tourenleiter', $storagePid);
	}
	$this->cacheService->clearPageCache();
        $this->redirect('listEdit');
    }
    
    /**
     * action edit
     *
     * @param \BeatHeim\HibTourenplanung\Domain\Model\Tourenleiter $tourenleiter
     * @param \BeatHeim\HibTourenplanung\Domain\Model\Tour $tour
     * @TYPO3\CMS\Extbase\Annotation\IgnoreValidation("tourenleiter")
     * @TYPO3\CMS\Extbase\Annotation\IgnoreValidation("tour")
     * @return void
     */
    public function editAction(\BeatHeim\HibTourenplanung\Domain\Model\Tourenleiter $tourenleiter, \BeatHeim\HibTourenplanung\Domain\Model\Tour $tour = NULL)
    {
        $this->view->assign('tourenleiter', $tourenleiter);
	$this->view->assign('types', $this->getTypes());
	$this->view->assign('tour', $tour);
    }
    
    /**
     * action update
     *
     * @param \BeatHeim\HibTourenplanung\Domain\Model\Tourenleiter $tourenleiter
     * @param \BeatHeim\HibTourenplanung\Domain\Model\Tour $tour
     * @TYPO3\CMS\Extbase\Annotation\IgnoreValidation("tourenleiter")
     * @TYPO3\CMS\Extbase\Annotation\IgnoreValidation("tour")
     * @return void
     */
    public function updateAction(\BeatHeim\HibTourenplanung\Domain\Model\Tourenleiter $tourenleiter, \BeatHeim\HibTourenplanung\Domain\Model\Tour $tour = NULL)
    {
        //$this->addFlashMessage('The object was updated. Please be aware that this action is publicly accessible unless you implement an access check. See http://wiki.typo3.org/T3Doc/Extension_Builder/Using_the_Extension_Builder#1._Model_the_domain', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::ERROR);
        $this->tourenleiterRepository->update($tourenleiter);
	//\TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($this->arguments);
	// BILD
	$newImagePath = 'fotos/tourenleiter';

	if ($_FILES['tx_hibtourenplanung_tourlist']['name']['picture'][0]) {
	    if ($tourenleiter->getPicture() != NULL)
	    {
		//remove old file first
		$resourceFactory = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Resource\\ResourceFactory');
		$fileReferenceObject = $resourceFactory->getFileReferenceObject($tourenleiter->getPicture()->getUid());
		$fileWasDeleted = $fileReferenceObject->getOriginalFile()->delete();
	    }

	    //be careful - you should validate the file type! This is not included here       
	    $tmpName = $_FILES['tx_hibtourenplanung_tourlist']['name']['picture'][0];
	    $tmpFile  = $_FILES['tx_hibtourenplanung_tourlist']['tmp_name']['picture'][0];

	    $storageRepository = $this->objectManager->get('TYPO3\\CMS\\Core\\Resource\\StorageRepository');
	    $storage = $storageRepository->findByUid('1'); //this is the fileadmin storage
	    //build the new storage folder
	    $targetFolder = $storage->getFolder($newImagePath);
	    $newFileName = $tmpName;

	    //build sys_file
	    $movedNewFile = $storage->addFile($tmpFile, $targetFolder, $newFileName);
	    $this->objectManager->get('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\PersistenceManager')->persistAll();
	    $storagePid = $tourenleiter->getPid();
	    //now we build the file reference
	    //see private function anotiations!
	    $this->buildRelations($tourenleiter->getUid(), $movedNewFile, 'picture', 'tx_hibtourenplanung_domain_model_tourenleiter', $storagePid);
	}
	
	$uriBuilder = $this->controllerContext->getUriBuilder();
	$uriBuilder->reset();
	$this->cacheService->clearPageCache();
	if ($tour)
	{
	    // specify the page ID for the link
	    $uriBuilder->setTargetPageUid($this->settings['tourEditPage']);
	    $year = $tour->getStartdate()->format('Y');
	    $uriBuilder->setArguments(array(
		'tx_hibtourenplanung_tourlist' => array(
		'year' => $year)));
	    $uriBuilder->setSection('collapse'.$tour->getUid());
	    $uri = $uriBuilder->build();
	    $this->redirectToUri($uri);
	}
	else 
	{
	    // specify the page ID for the link
	    $uriBuilder->setTargetPageUid($this->settings['tourenleiterEditPage']);
	    $uriBuilder->setSection('collapse'.$tourenleiter->getUid());
	    $uri = $uriBuilder->build();
	    $this->redirectToUri($uri);
	}
        //$this->redirect('listEdit', 'Tour');
    }
    
    /**
     * action delete
     *
     * @param \BeatHeim\HibTourenplanung\Domain\Model\Tourenleiter $tourenleiter
     * @return void
     */
    public function deleteAction(\BeatHeim\HibTourenplanung\Domain\Model\Tourenleiter $tourenleiter)
    {
        $this->addFlashMessage('The object was deleted. Please be aware that this action is publicly accessible unless you implement an access check. See http://wiki.typo3.org/T3Doc/Extension_Builder/Using_the_Extension_Builder#1._Model_the_domain', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::ERROR);
        $this->tourenleiterRepository->remove($tourenleiter);
        $this->redirect('listEdit');
    }
    
     /**
     * Build relations for FAL
     * 
     * @param int    $newStorageUid //The UID of the  model
     * @param array  $file //The file model of the image
     * @param string $field //the name of the relation field
     * @param string $table //the table of the model
     */
    private function buildRelations($newStorageUid, $file, $field, $table, $storagePid) {

        $data = array();
        $data['sys_file_reference']['NEW1234'] = array(
            'uid_local' => $file->getUid(),
            'uid_foreign' => $newStorageUid, // uid of your content record or own model
            'tablenames' => $table, //tca table name
            'fieldname' => $field, //see tca for fieldname
            'pid' => $storagePid,
            'table_local' => 'sys_file',
        );
        $data[$table][$newStorageUid] = array('image' => 'NEW1234'); //this is needed, i dont know why :( but not stored in tables

        /** @var \TYPO3\CMS\Core\DataHandling\DataHandler $tce */
        $tce = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Core\DataHandling\DataHandler'); // create TCE instance
        $tce->start($data, array());
        $tce->process_datamap();
    }

        /**
    * prepare types for select box
    *
    * @return array
    */
    public function getTypes() {
	$types = array();
	
	$type = new \stdClass();
	$type->key = '1';
	$type->value = 'Tourenleiter';
	$types[] = $type;
	$type = new \stdClass();
	$type->key = '2';
	$type->value = 'Tourenleiter & Vorstand';
	$types[] = $type;
	$type = new \stdClass();
	$type->key = '3';
	$type->value = 'Vorstand';
	$types[] = $type;
	$type = new \stdClass();
	$type->key = '4';
	$type->value = 'Nur Schneesportleiter';
	$types[] = $type;
	return $types;
    }
}