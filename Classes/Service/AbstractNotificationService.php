<?php

namespace BeatHeim\HibTourenplanung\Service;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013-2016 Felix Nagel <info@felixnagel.com>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use TYPO3\T3extblog\Domain\Model\AbstractSubscriber;
use TYPO3\T3extblog\Domain\Model\Comment;

/**
 * Handles all notification mails
 */
abstract class AbstractNotificationService implements NotificationServiceInterface, SingletonInterface {

	/**
	 * @var \TYPO3\CMS\Extbase\Object\ObjectManagerInterface
	 * @inject
	 */
	protected $objectManager;



	/**
	 * @var \BeatHeim\HibTourenplanung\Service\Service\EmailService
	 * @inject
	 */
	protected $emailService;


	/**
	 * @return void
	 */
	public function initializeObject() {
		//$this->settings = $this->settingsService->getTypoScriptSettings();
	}

	/**
	 * Send subscriber emails
	 *
	 * @param AbstractSubscriber $subscriber
	 * @param string $subject
	 * @param string $template
	 * @param array $variables
	 *
	 * @return void
	 */
	protected function sendEmail(AbstractSubscriber $subscriber, $subject, $template, $emailBody {


		$this->emailService->send(
			$subscriber->getMailTo(),
			array($settings['mailFrom']['email'] => $settings['mailFrom']['name']),
			$subject,
			$emailBody
		);
	}

	/**
	 * Render dateTime object for using in template
	 *
	 * @todo We probably want to move this back to Fluid
	 *       Using a format:date VH stopped working with 7.4
	 *
	 * @return \DateTime
	 */
	protected function getValidUntil() {
		$date = new \DateTime();
		$modify = '+1 hour';

		if (isset($this->subscriptionSettings['subscriber']['emailHashTimeout'])) {
			$modify = trim($this->subscriptionSettings['subscriber']['emailHashTimeout']);
		}

		$date->modify($modify);

		return $date;
	}





}
