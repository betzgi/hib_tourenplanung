<?php

namespace BeatHeim\HibTourenplanung\Service;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013-2016 Felix Nagel <info@felixnagel.com>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\MailUtility;
use TYPO3\CMS\Fluid\View\StandaloneView;

/**
 * Handles email sending and templating
 */
class EmailService implements SingletonInterface {

	/**
	 * Extension name
	 *
	 * @var string
	 */
	protected $extensionName = 'hibtourenplanung';

	/**
	 * @var \TYPO3\CMS\Extbase\Object\ObjectManagerInterface
	 * @TYPO3\CMS\Extbase\Annotation\Inject`
	 */
	protected $objectManager;

	/**
	 * This is the main-function for sending Mails
	 *
	 * @param array $mailTo
	 * @param array $mailFrom
	 * @param string $subject
	 * @param string $emailBody
	 *
	 * @return integer the number of recipients who were accepted for delivery
	 */
	public function send($mailTo, $mailFrom, $subject, $emailBody) {
		if (!($mailTo && is_array($mailTo) && GeneralUtility::validEmail(key($mailTo)))) {
			//$this->log->error('Given mailto email address is invalid.', $mailTo);

			return FALSE;
		}

		if (!($mailFrom && is_array($mailFrom) && GeneralUtility::validEmail(key($mailFrom)))) {
			$mailFrom = MailUtility::getSystemFrom();
		}

		/* @var $message \TYPO3\CMS\Core\Mail\MailMessage */
		$message = $this->objectManager->get('TYPO3\\CMS\\Core\\Mail\\MailMessage');
		$message
			->setTo($mailTo)
			->setFrom($mailFrom)
			->setSubject($subject)
			->setCharset(\TYPO3\T3extblog\Utility\GeneralUtility::getTsFe()->metaCharset);

		// Plain text only
		if (strip_tags($emailBody) == $emailBody) {
			$message->setBody($emailBody, 'text/plain');
		} else {
			// Send as HTML and plain text
			$message->setBody($emailBody, 'text/html');
			$message->addPart($this->preparePlainTextBody($emailBody), 'text/plain' );
		}

		if (!$this->settings['debug']['disableEmailTransmission']) {
			$message->send();
		}

		$logData = array(
			'mailTo' => $mailTo,
			'mailFrom' => $mailFrom,
			'subject' => $subject,
			'emailBody' => $emailBody,
			'isSent' => $message->isSent()
		);
		//$this->log->dev('Email sent.', $logData);

		return $logData['isSent'];
	}


	/**
	 * Prepare html as plain text
	 *
	 * @param string $html
	 *
	 * @return string
	 */
	protected function preparePlainTextBody($html) {
		$output = preg_replace('/<style\\b[^>]*>(.*?)<\\/style>/s', '', $html);
		$output = strip_tags(preg_replace('/<a.* href=(?:"|\')(.*)(?:"|\').*>/', '$1', $output));
		$output = GeneralUtility::substUrlsInPlainText($output);
		$output = MailUtility::breakLinesForEmail($output);
		$output = preg_replace('/(?:(?:\r\n|\r|\n)\s*){2}/s', "\n\n", $output);

		return $output;
	}
}
