<?php
namespace BeatHeim\HibTourenplanung\Validation\Validator;
/**
 * Validator Tx_Hibtourenplanung_Domain_Validator_NotEmptyValidator
 *
 * @package Ivoteevents
 * @version $Id$
 * @scope prototype
 */
class StartdateValidator extends \TYPO3\CMS\Extbase\Validation\Validator\AbstractValidator {

    
    public function isValid($property) {
        $this->errors = array();

        if ($property === NULL || $property === '') {
            $this->addError('Datum darf nicht leer sein',1383400016);
            return false;
        }

        return true;
    }  
    
}
?>