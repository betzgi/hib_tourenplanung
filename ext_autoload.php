<?php

$extensionPath = t3lib_extMgm::extPath('hib_tourenplanung');
return array(
    'Tx_hibTourenplanung_Task' => t3lib_extMgm::extPath('hib_tourenplanung', 'Tasks/Task.php'),
    'Tx_hibTourenplanung_TaskDetail' => t3lib_extMgm::extPath('hib_tourenplanung', 'Tasks/TaskDetail.php')
);

?>
