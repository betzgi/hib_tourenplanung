<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'BeatHeim.' . $_EXTKEY,
	'Tourlist',
	array(
		'Tour' => 'list, listEdit, listPrint, listPrintProv, listPrintParticipant, emailParticipant, sendEmailParticipant, show, new, create, edit, update, delete, toggleState, statistik',
		'Report' => 'list, listEdit, show, new, create, edit, update, delete,listPhoto, newPhoto, uploadPhoto, deletePhoto, toggleState',
		'Tourenleiter' => 'list, listEdit, listVorstand, show, new, create, edit, update, delete',
		'Participant' => 'list, show, new, create, edit, update, delete, check, newMulti, createMulti',
	        'Member' => 'list,edit',
		
	),
	// non-cacheable actions
	array(
		'Tour' => 'new, create, update, delete, toggleState, listEdit, emailParticipant, sendEmailParticipant',
		'Report' => 'create, update, delete, listEdit, newPhoto, deletePhoto, uploadPhoto, toggleState',
		'Tourenleiter' => 'create, update, delete',
		'Participant' => 'new, create, update, delete, check, newMulti, createMulti',
	        'Member' => 'update',
		
	)
);

  $TYPO3_CONF_VARS['SC_OPTIONS']['scheduler']['tasks']['BeatHeim\\HibTourenplanung\\Tasks\\Task'] = array(
    'extension' => $_EXTKEY,
    'title' => 'ReminderTask',
    'description' => 'reminds Tourenleiter to create Tour report'
  );
  
  $TYPO3_CONF_VARS['SC_OPTIONS']['scheduler']['tasks']['BeatHeim\\HibTourenplanung\\Tasks\\TaskDetail'] = array(
    'extension' => $_EXTKEY,
    'title' => 'DetailTask',
    'description' => 'reminds Tourenleiter to specify Tour details'
  );
