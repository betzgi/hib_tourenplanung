<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
	'BeatHeim.' . $_EXTKEY,
	'Tourlist',
	'Touren Liste'
);

//\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
//	'BeatHeim.' . $_EXTKEY,
//	'Tourleiterlist',
//	'Tourenleiter Liste'
//);

$pluginSignature = str_replace('_','',$_EXTKEY) . '_tourlist';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($pluginSignature, 'FILE:EXT:' . $_EXTKEY . '/Configuration/FlexForms/flexform_tourlist.xml');

if (TYPO3_MODE === 'BE') {

//	/**
//	 * Registers a Backend Module
//	 */
//	\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
//		'BeatHeim.' . $_EXTKEY,
//		'web',	 // Make module a submodule of 'web'
//		'tormanager',	// Submodule key
//		'',						// Position
//		array(
//			'Tour' => 'list, show, new, create, edit, update, delete','Report' => 'list, show, new, create, edit, update, delete','Tourenleiter' => 'list, show, new, create, edit, update, delete','Participant' => 'list, show, new, create, edit, update, delete, check',
//		),
//		array(
//			'access' => 'user,group',
//			'icon'   => 'EXT:' . $_EXTKEY . '/ext_icon.gif',
//			'labels' => 'LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/locallang_tormanager.xlf',
//		)
//	);

}

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'Tourenplanung');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hibtourenplanung_domain_model_tour', 'EXT:hib_tourenplanung/Resources/Private/Language/locallang_csh_tx_hibtourenplanung_domain_model_tour.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hibtourenplanung_domain_model_tour');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hibtourenplanung_domain_model_report', 'EXT:hib_tourenplanung/Resources/Private/Language/locallang_csh_tx_hibtourenplanung_domain_model_report.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hibtourenplanung_domain_model_report');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hibtourenplanung_domain_model_tourenleiter', 'EXT:hib_tourenplanung/Resources/Private/Language/locallang_csh_tx_hibtourenplanung_domain_model_tourenleiter.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hibtourenplanung_domain_model_tourenleiter');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hibtourenplanung_domain_model_participant', 'EXT:hib_tourenplanung/Resources/Private/Language/locallang_csh_tx_hibtourenplanung_domain_model_participant.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hibtourenplanung_domain_model_participant');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hibtourenplanung_domain_model_member', 'EXT:hib_tourenplanung/Resources/Private/Language/locallang_csh_tx_hibtourenplanung_domain_model_member.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hibtourenplanung_domain_model_member');
