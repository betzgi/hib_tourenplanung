<?php

namespace BeatHeim\HibTourenplanung\Tests\Unit\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 Beat Heim <betzgi@gmail.com>
 *           Beat Heim <betzgi@gmail.com>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class \BeatHeim\HibTourenplanung\Domain\Model\Tour.
 *
 * @copyright Copyright belongs to the respective authors
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 * @author Beat Heim <betzgi@gmail.com>
 * @author Beat Heim <betzgi@gmail.com>
 */
class TourTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
	/**
	 * @var \BeatHeim\HibTourenplanung\Domain\Model\Tour
	 */
	protected $subject = NULL;

	public function setUp()
	{
		$this->subject = new \BeatHeim\HibTourenplanung\Domain\Model\Tour();
	}

	public function tearDown()
	{
		unset($this->subject);
	}

	/**
	 * @test
	 */
	public function getTitleReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getTitle()
		);
	}

	/**
	 * @test
	 */
	public function setTitleForStringSetsTitle()
	{
		$this->subject->setTitle('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'title',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getTypeReturnsInitialValueForInt()
	{	}

	/**
	 * @test
	 */
	public function setTypeForIntSetsType()
	{	}

	/**
	 * @test
	 */
	public function getStartdateReturnsInitialValueForDateTime()
	{
		$this->assertEquals(
			NULL,
			$this->subject->getStartdate()
		);
	}

	/**
	 * @test
	 */
	public function setStartdateForDateTimeSetsStartdate()
	{
		$dateTimeFixture = new \DateTime();
		$this->subject->setStartdate($dateTimeFixture);

		$this->assertAttributeEquals(
			$dateTimeFixture,
			'startdate',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getEnddateReturnsInitialValueForDateTime()
	{
		$this->assertEquals(
			NULL,
			$this->subject->getEnddate()
		);
	}

	/**
	 * @test
	 */
	public function setEnddateForDateTimeSetsEnddate()
	{
		$dateTimeFixture = new \DateTime();
		$this->subject->setEnddate($dateTimeFixture);

		$this->assertAttributeEquals(
			$dateTimeFixture,
			'enddate',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getDifficultyReturnsInitialValueForInt()
	{	}

	/**
	 * @test
	 */
	public function setDifficultyForIntSetsDifficulty()
	{	}

	/**
	 * @test
	 */
	public function getDescriptionReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getDescription()
		);
	}

	/**
	 * @test
	 */
	public function setDescriptionForStringSetsDescription()
	{
		$this->subject->setDescription('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'description',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getRequirementsReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getRequirements()
		);
	}

	/**
	 * @test
	 */
	public function setRequirementsForStringSetsRequirements()
	{
		$this->subject->setRequirements('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'requirements',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getEquipmentReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getEquipment()
		);
	}

	/**
	 * @test
	 */
	public function setEquipmentForStringSetsEquipment()
	{
		$this->subject->setEquipment('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'equipment',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getMeetingtimeReturnsInitialValueForInt()
	{	}

	/**
	 * @test
	 */
	public function setMeetingtimeForIntSetsMeetingtime()
	{	}

	/**
	 * @test
	 */
	public function getMeetingReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getMeeting()
		);
	}

	/**
	 * @test
	 */
	public function setMeetingForStringSetsMeeting()
	{
		$this->subject->setMeeting('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'meeting',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getReturningtimeReturnsInitialValueForInt()
	{	}

	/**
	 * @test
	 */
	public function setReturningtimeForIntSetsReturningtime()
	{	}

	/**
	 * @test
	 */
	public function getReturningReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getReturning()
		);
	}

	/**
	 * @test
	 */
	public function setReturningForStringSetsReturning()
	{
		$this->subject->setReturning('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'returning',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getSigninReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getSignin()
		);
	}

	/**
	 * @test
	 */
	public function setSigninForStringSetsSignin()
	{
		$this->subject->setSignin('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'signin',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getInfoReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getInfo()
		);
	}

	/**
	 * @test
	 */
	public function setInfoForStringSetsInfo()
	{
		$this->subject->setInfo('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'info',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getCostReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getCost()
		);
	}

	/**
	 * @test
	 */
	public function setCostForStringSetsCost()
	{
		$this->subject->setCost('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'cost',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getSpecialReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getSpecial()
		);
	}

	/**
	 * @test
	 */
	public function setSpecialForStringSetsSpecial()
	{
		$this->subject->setSpecial('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'special',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getStateReturnsInitialValueForInt()
	{	}

	/**
	 * @test
	 */
	public function setStateForIntSetsState()
	{	}

	/**
	 * @test
	 */
	public function getEregisterReturnsInitialValueForBool()
	{
		$this->assertSame(
			FALSE,
			$this->subject->getEregister()
		);
	}

	/**
	 * @test
	 */
	public function setEregisterForBoolSetsEregister()
	{
		$this->subject->setEregister(TRUE);

		$this->assertAttributeEquals(
			TRUE,
			'eregister',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getTourenleiterReturnsInitialValueForTourenleiter()
	{
		$this->assertEquals(
			NULL,
			$this->subject->getTourenleiter()
		);
	}

	/**
	 * @test
	 */
	public function setTourenleiterForTourenleiterSetsTourenleiter()
	{
		$tourenleiterFixture = new \BeatHeim\HibTourenplanung\Domain\Model\Tourenleiter();
		$this->subject->setTourenleiter($tourenleiterFixture);

		$this->assertAttributeEquals(
			$tourenleiterFixture,
			'tourenleiter',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getSubscriberReturnsInitialValueForParticipant()
	{
		$newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->assertEquals(
			$newObjectStorage,
			$this->subject->getSubscriber()
		);
	}

	/**
	 * @test
	 */
	public function setSubscriberForObjectStorageContainingParticipantSetsSubscriber()
	{
		$subscriber = new \BeatHeim\HibTourenplanung\Domain\Model\Participant();
		$objectStorageHoldingExactlyOneSubscriber = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$objectStorageHoldingExactlyOneSubscriber->attach($subscriber);
		$this->subject->setSubscriber($objectStorageHoldingExactlyOneSubscriber);

		$this->assertAttributeEquals(
			$objectStorageHoldingExactlyOneSubscriber,
			'subscriber',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function addSubscriberToObjectStorageHoldingSubscriber()
	{
		$subscriber = new \BeatHeim\HibTourenplanung\Domain\Model\Participant();
		$subscriberObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('attach'), array(), '', FALSE);
		$subscriberObjectStorageMock->expects($this->once())->method('attach')->with($this->equalTo($subscriber));
		$this->inject($this->subject, 'subscriber', $subscriberObjectStorageMock);

		$this->subject->addSubscriber($subscriber);
	}

	/**
	 * @test
	 */
	public function removeSubscriberFromObjectStorageHoldingSubscriber()
	{
		$subscriber = new \BeatHeim\HibTourenplanung\Domain\Model\Participant();
		$subscriberObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('detach'), array(), '', FALSE);
		$subscriberObjectStorageMock->expects($this->once())->method('detach')->with($this->equalTo($subscriber));
		$this->inject($this->subject, 'subscriber', $subscriberObjectStorageMock);

		$this->subject->removeSubscriber($subscriber);

	}
}
