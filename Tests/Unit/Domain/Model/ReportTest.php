<?php

namespace BeatHeim\HibTourenplanung\Tests\Unit\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 Beat Heim <betzgi@gmail.com>
 *           Beat Heim <betzgi@gmail.com>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class \BeatHeim\HibTourenplanung\Domain\Model\Report.
 *
 * @copyright Copyright belongs to the respective authors
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 * @author Beat Heim <betzgi@gmail.com>
 * @author Beat Heim <betzgi@gmail.com>
 */
class ReportTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
	/**
	 * @var \BeatHeim\HibTourenplanung\Domain\Model\Report
	 */
	protected $subject = NULL;

	public function setUp()
	{
		$this->subject = new \BeatHeim\HibTourenplanung\Domain\Model\Report();
	}

	public function tearDown()
	{
		unset($this->subject);
	}

	/**
	 * @test
	 */
	public function getTitleReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getTitle()
		);
	}

	/**
	 * @test
	 */
	public function setTitleForStringSetsTitle()
	{
		$this->subject->setTitle('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'title',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getShorttextReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getShorttext()
		);
	}

	/**
	 * @test
	 */
	public function setShorttextForStringSetsShorttext()
	{
		$this->subject->setShorttext('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'shorttext',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getTextReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getText()
		);
	}

	/**
	 * @test
	 */
	public function setTextForStringSetsText()
	{
		$this->subject->setText('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'text',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getStateReturnsInitialValueForInt()
	{	}

	/**
	 * @test
	 */
	public function setStateForIntSetsState()
	{	}

	/**
	 * @test
	 */
	public function getAuthorReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getAuthor()
		);
	}

	/**
	 * @test
	 */
	public function setAuthorForStringSetsAuthor()
	{
		$this->subject->setAuthor('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'author',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getPictureReturnsInitialValueForFileReference()
	{
		$this->assertEquals(
			NULL,
			$this->subject->getPicture()
		);
	}

	/**
	 * @test
	 */
	public function setPictureForFileReferenceSetsPicture()
	{
		$fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
		$this->subject->setPicture($fileReferenceFixture);

		$this->assertAttributeEquals(
			$fileReferenceFixture,
			'picture',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getTourReturnsInitialValueForTour()
	{
		$this->assertEquals(
			NULL,
			$this->subject->getTour()
		);
	}

	/**
	 * @test
	 */
	public function setTourForTourSetsTour()
	{
		$tourFixture = new \BeatHeim\HibTourenplanung\Domain\Model\Tour();
		$this->subject->setTour($tourFixture);

		$this->assertAttributeEquals(
			$tourFixture,
			'tour',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getParticipantReturnsInitialValueForParticipant()
	{
		$newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->assertEquals(
			$newObjectStorage,
			$this->subject->getParticipant()
		);
	}

	/**
	 * @test
	 */
	public function setParticipantForObjectStorageContainingParticipantSetsParticipant()
	{
		$participant = new \BeatHeim\HibTourenplanung\Domain\Model\Participant();
		$objectStorageHoldingExactlyOneParticipant = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$objectStorageHoldingExactlyOneParticipant->attach($participant);
		$this->subject->setParticipant($objectStorageHoldingExactlyOneParticipant);

		$this->assertAttributeEquals(
			$objectStorageHoldingExactlyOneParticipant,
			'participant',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function addParticipantToObjectStorageHoldingParticipant()
	{
		$participant = new \BeatHeim\HibTourenplanung\Domain\Model\Participant();
		$participantObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('attach'), array(), '', FALSE);
		$participantObjectStorageMock->expects($this->once())->method('attach')->with($this->equalTo($participant));
		$this->inject($this->subject, 'participant', $participantObjectStorageMock);

		$this->subject->addParticipant($participant);
	}

	/**
	 * @test
	 */
	public function removeParticipantFromObjectStorageHoldingParticipant()
	{
		$participant = new \BeatHeim\HibTourenplanung\Domain\Model\Participant();
		$participantObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('detach'), array(), '', FALSE);
		$participantObjectStorageMock->expects($this->once())->method('detach')->with($this->equalTo($participant));
		$this->inject($this->subject, 'participant', $participantObjectStorageMock);

		$this->subject->removeParticipant($participant);

	}
}
