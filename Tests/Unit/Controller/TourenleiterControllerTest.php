<?php
namespace BeatHeim\HibTourenplanung\Tests\Unit\Controller;
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 Beat Heim <betzgi@gmail.com>
 *  			Beat Heim <betzgi@gmail.com>
 *  			
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class BeatHeim\HibTourenplanung\Controller\TourenleiterController.
 *
 * @author Beat Heim <betzgi@gmail.com>
 * @author Beat Heim <betzgi@gmail.com>
 */
class TourenleiterControllerTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{

	/**
	 * @var \BeatHeim\HibTourenplanung\Controller\TourenleiterController
	 */
	protected $subject = NULL;

	public function setUp()
	{
		$this->subject = $this->getMock('BeatHeim\\HibTourenplanung\\Controller\\TourenleiterController', array('redirect', 'forward', 'addFlashMessage'), array(), '', FALSE);
	}

	public function tearDown()
	{
		unset($this->subject);
	}

	/**
	 * @test
	 */
	public function listActionFetchesAllTourenleitersFromRepositoryAndAssignsThemToView()
	{

		$allTourenleiters = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array(), array(), '', FALSE);

		$tourenleiterRepository = $this->getMock('BeatHeim\\HibTourenplanung\\Domain\\Repository\\TourenleiterRepository', array('findAll'), array(), '', FALSE);
		$tourenleiterRepository->expects($this->once())->method('findAll')->will($this->returnValue($allTourenleiters));
		$this->inject($this->subject, 'tourenleiterRepository', $tourenleiterRepository);

		$view = $this->getMock('TYPO3\\CMS\\Extbase\\Mvc\\View\\ViewInterface');
		$view->expects($this->once())->method('assign')->with('tourenleiters', $allTourenleiters);
		$this->inject($this->subject, 'view', $view);

		$this->subject->listAction();
	}

	/**
	 * @test
	 */
	public function showActionAssignsTheGivenTourenleiterToView()
	{
		$tourenleiter = new \BeatHeim\HibTourenplanung\Domain\Model\Tourenleiter();

		$view = $this->getMock('TYPO3\\CMS\\Extbase\\Mvc\\View\\ViewInterface');
		$this->inject($this->subject, 'view', $view);
		$view->expects($this->once())->method('assign')->with('tourenleiter', $tourenleiter);

		$this->subject->showAction($tourenleiter);
	}

	/**
	 * @test
	 */
	public function createActionAddsTheGivenTourenleiterToTourenleiterRepository()
	{
		$tourenleiter = new \BeatHeim\HibTourenplanung\Domain\Model\Tourenleiter();

		$tourenleiterRepository = $this->getMock('BeatHeim\\HibTourenplanung\\Domain\\Repository\\TourenleiterRepository', array('add'), array(), '', FALSE);
		$tourenleiterRepository->expects($this->once())->method('add')->with($tourenleiter);
		$this->inject($this->subject, 'tourenleiterRepository', $tourenleiterRepository);

		$this->subject->createAction($tourenleiter);
	}

	/**
	 * @test
	 */
	public function editActionAssignsTheGivenTourenleiterToView()
	{
		$tourenleiter = new \BeatHeim\HibTourenplanung\Domain\Model\Tourenleiter();

		$view = $this->getMock('TYPO3\\CMS\\Extbase\\Mvc\\View\\ViewInterface');
		$this->inject($this->subject, 'view', $view);
		$view->expects($this->once())->method('assign')->with('tourenleiter', $tourenleiter);

		$this->subject->editAction($tourenleiter);
	}

	/**
	 * @test
	 */
	public function updateActionUpdatesTheGivenTourenleiterInTourenleiterRepository()
	{
		$tourenleiter = new \BeatHeim\HibTourenplanung\Domain\Model\Tourenleiter();

		$tourenleiterRepository = $this->getMock('BeatHeim\\HibTourenplanung\\Domain\\Repository\\TourenleiterRepository', array('update'), array(), '', FALSE);
		$tourenleiterRepository->expects($this->once())->method('update')->with($tourenleiter);
		$this->inject($this->subject, 'tourenleiterRepository', $tourenleiterRepository);

		$this->subject->updateAction($tourenleiter);
	}

	/**
	 * @test
	 */
	public function deleteActionRemovesTheGivenTourenleiterFromTourenleiterRepository()
	{
		$tourenleiter = new \BeatHeim\HibTourenplanung\Domain\Model\Tourenleiter();

		$tourenleiterRepository = $this->getMock('BeatHeim\\HibTourenplanung\\Domain\\Repository\\TourenleiterRepository', array('remove'), array(), '', FALSE);
		$tourenleiterRepository->expects($this->once())->method('remove')->with($tourenleiter);
		$this->inject($this->subject, 'tourenleiterRepository', $tourenleiterRepository);

		$this->subject->deleteAction($tourenleiter);
	}
}
