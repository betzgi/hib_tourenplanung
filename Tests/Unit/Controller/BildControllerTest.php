<?php
namespace BeatHeim\HibTourenplanung\Tests\Unit\Controller;
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 Beat Heim <betzgi@gmail.com>
 *  			
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class BeatHeim\HibTourenplanung\Controller\BildController.
 *
 * @author Beat Heim <betzgi@gmail.com>
 */
class BildControllerTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{

	/**
	 * @var \BeatHeim\HibTourenplanung\Controller\BildController
	 */
	protected $subject = NULL;

	public function setUp()
	{
		$this->subject = $this->getMock('BeatHeim\\HibTourenplanung\\Controller\\BildController', array('redirect', 'forward', 'addFlashMessage'), array(), '', FALSE);
	}

	public function tearDown()
	{
		unset($this->subject);
	}

	/**
	 * @test
	 */
	public function listActionFetchesAllBildsFromRepositoryAndAssignsThemToView()
	{

		$allBilds = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array(), array(), '', FALSE);

		$bildRepository = $this->getMock('BeatHeim\\HibTourenplanung\\Domain\\Repository\\BildRepository', array('findAll'), array(), '', FALSE);
		$bildRepository->expects($this->once())->method('findAll')->will($this->returnValue($allBilds));
		$this->inject($this->subject, 'bildRepository', $bildRepository);

		$view = $this->getMock('TYPO3\\CMS\\Extbase\\Mvc\\View\\ViewInterface');
		$view->expects($this->once())->method('assign')->with('bilds', $allBilds);
		$this->inject($this->subject, 'view', $view);

		$this->subject->listAction();
	}

	/**
	 * @test
	 */
	public function showActionAssignsTheGivenBildToView()
	{
		$bild = new \BeatHeim\HibTourenplanung\Domain\Model\Bild();

		$view = $this->getMock('TYPO3\\CMS\\Extbase\\Mvc\\View\\ViewInterface');
		$this->inject($this->subject, 'view', $view);
		$view->expects($this->once())->method('assign')->with('bild', $bild);

		$this->subject->showAction($bild);
	}

	/**
	 * @test
	 */
	public function createActionAddsTheGivenBildToBildRepository()
	{
		$bild = new \BeatHeim\HibTourenplanung\Domain\Model\Bild();

		$bildRepository = $this->getMock('BeatHeim\\HibTourenplanung\\Domain\\Repository\\BildRepository', array('add'), array(), '', FALSE);
		$bildRepository->expects($this->once())->method('add')->with($bild);
		$this->inject($this->subject, 'bildRepository', $bildRepository);

		$this->subject->createAction($bild);
	}

	/**
	 * @test
	 */
	public function editActionAssignsTheGivenBildToView()
	{
		$bild = new \BeatHeim\HibTourenplanung\Domain\Model\Bild();

		$view = $this->getMock('TYPO3\\CMS\\Extbase\\Mvc\\View\\ViewInterface');
		$this->inject($this->subject, 'view', $view);
		$view->expects($this->once())->method('assign')->with('bild', $bild);

		$this->subject->editAction($bild);
	}

	/**
	 * @test
	 */
	public function updateActionUpdatesTheGivenBildInBildRepository()
	{
		$bild = new \BeatHeim\HibTourenplanung\Domain\Model\Bild();

		$bildRepository = $this->getMock('BeatHeim\\HibTourenplanung\\Domain\\Repository\\BildRepository', array('update'), array(), '', FALSE);
		$bildRepository->expects($this->once())->method('update')->with($bild);
		$this->inject($this->subject, 'bildRepository', $bildRepository);

		$this->subject->updateAction($bild);
	}

	/**
	 * @test
	 */
	public function deleteActionRemovesTheGivenBildFromBildRepository()
	{
		$bild = new \BeatHeim\HibTourenplanung\Domain\Model\Bild();

		$bildRepository = $this->getMock('BeatHeim\\HibTourenplanung\\Domain\\Repository\\BildRepository', array('remove'), array(), '', FALSE);
		$bildRepository->expects($this->once())->method('remove')->with($bild);
		$this->inject($this->subject, 'bildRepository', $bildRepository);

		$this->subject->deleteAction($bild);
	}
}
